// Copyright © 2015, Filipe Verri and Paulo Urio.
#include "./file.h"

#include <sys/stat.h>

#include <cstddef>

#include <string>

namespace util {

File::File(const std::string& filename) : file_(filename) {}

std::string File::path() const {
  const size_t last_slash_idx = filename().rfind(kDirectorySeparator);
  std::string directory;

  if (std::string::npos != last_slash_idx) {
    directory = filename().substr(0, last_slash_idx);
  }
  return directory;
}

std::string File::ext() const {
  std::size_t pos = filename().find_last_of(kExtensionSeparator);
  std::size_t dpos = filename().find_last_of(kDirectorySeparator);
  if (pos == std::string::npos || (dpos != std::string::npos && dpos > pos))
    return std::string("");
  return filename().substr(pos + 1);
}

std::string File::basename() const {
  std::size_t pos = filename().find_last_of(kDirectorySeparator);
  if (pos == std::string::npos) return file_;
  return filename().substr(pos + 1);
}

bool File::exists() const {
  struct stat buffer;

  return (stat(filename().c_str(), &buffer) == 0);
}

bool File::ensure_path() const {
  std::string directory = path();
  int ret = 1;

  if (directory.size() > 0 && !exists()) {
    ret = ::mkdir(directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  }
  return ret == 0;
}

}  // namespace util
