// Copyright © 2015, Filipe Verri and Paulo Urio.
#ifndef PCA_H_
#define PCA_H_

#include <opencv2/core/core.hpp>

#include <cstddef>

#include <vector>

#include "./coord.h"

namespace ip {

class PCA {
 public:
  using EigenVector = std::vector<Coord_<double>>;

  explicit PCA(const std::vector<Coord>& boundary, std::size_t components = 2);

  ~PCA() = default;

  const EigenVector& vectors() const { return eigenvectors_; }

  const std::vector<double>& values() const { return eigenvalues_; }

  Coord center() const { return object_pos_; }

  double magnitude(std::size_t index) const;

  double angle() const;

  Coord axis(std::size_t index) const;

  // Draw the principal components
  void draw_axes(cv::InputArray src, cv::OutputArray dst, double radius = 0.0);

 private:
  void compute();

  std::vector<Coord> points_;
  std::size_t components_;

  EigenVector eigenvectors_;
  std::vector<double> eigenvalues_;

  Coord object_pos_;
};

}  // namespace ip

#endif  // PCA_H_
