
extract() {
  /usr/bin/vendor_perl/exiftool $1 | grep "$2" | tr -s ' ' | cut -d ' ' -f 4
}

for f in *.jpg
do
  echo "$f `extract $f "^X Resolution"` `extract $f "^Y Resolution"`"
done

