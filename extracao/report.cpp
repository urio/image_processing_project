// Copyright © 2015, Filipe Verri and Paulo Urio.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cstdlib>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

#include "./features.h"
#include "./file.h"
#include "./descriptors.h"
#include "./options.h"
#include "./processing.h"

#include "./datatable.h"

#include "./circle.h"

static const std::string kOutputDir = "exemplo/";

static constexpr unsigned int kGrayLevels = 64;
static constexpr double kSigma = 0.1;
static constexpr std::size_t kNumberOfFourierDescriptors = 10;

static void yield(std::string file, const cv::Mat& img,
                  bool direct_call = false) {
  static int count;
  std::stringstream ss;
  ss << kOutputDir << std::setw(3) << std::setfill('0') << count;
  ss << '-' << file << ".jpg";
  count++;
  cv::Mat resized;
  double a = 512.0 / static_cast<double>(img.cols);
  cv::Size sz(static_cast<int>(img.cols * a), static_cast<int>(img.rows * a));
  cv::resize(img, resized, sz, cv::INTER_NEAREST);
  std::cout << "Writing " << ss.str() << std::endl;
  if (direct_call)
    cv::imwrite(ss.str(), resized);
  else
    ip::save(ss.str(), resized);
}

template <class T>
static void yield_normalized(std::ostream& out, const T& input_values,
                             bool log_scale = true) {
  std::vector<double> values;
  values.reserve(kNumberOfFourierDescriptors);
  for (auto& v : input_values) {
    double r = std::abs(v);
    if (log_scale) r = std::log(r);
    values.push_back(r);
  }
  util::rescale(values.begin(), values.end(), values.begin(), 0.0, 1.0);
  bool first = true;
  out << std::setprecision(5);
  double spacing = 1.0 / (kNumberOfFourierDescriptors + 12);
  for (auto& v : values) {
    if (!first) out << " & ";
    out << "\\cellcolor[gray]{" << v << "}";
    out << "\\hspace{" << spacing << "\\textwidth}";
    first = false;
  }
  out << "\\\\\n";
}

template <class C>
static void yield_tab(std::ostream& out, const C& v) {
  static_assert(!std::is_same<typename C::value_type, ip::Complex>::value,
                "can not yield complex container.");
  util::paste(std::begin(v), std::end(v), out, "\t");
  out << "\n";
}

template <class F, class C>
static auto convert(const C& container, F&& f)
    -> std::valarray<decltype(f(typename C::value_type{}))> {
  std::valarray<decltype(f(typename C::value_type{}))> result(container.size());
  std::transform(std::begin(container), std::end(container), std::begin(result),
                 f);
  return result;
}

int main(int argc, char* argv[]) {
  options::Opts opts(argc, argv);
  cv::Mat input, r;

  util::File(kOutputDir).ensure_path();

  // Loading and scaling
  ip::load(opts.input, input, CV_LOAD_IMAGE_GRAYSCALE, CV_8UC1);
  cv::copyMakeBorder(input, input, 3, 3, 3, 3, cv::BORDER_CONSTANT, 0xFF);
  yield("Input", input);

  // Scaling (ignored)

  // Padding, blurring, thresholding, and eroding
  cv::Mat pp;
  cv::copyMakeBorder(input, input, 3, 3, 3, 3, cv::BORDER_CONSTANT, 0xFF);
  cv::GaussianBlur(input, pp, cv::Size(3, 3), 2);
  yield("Gaussian-Blur", pp);
  cv::threshold(pp, pp, 0, 255, cv::THRESH_BINARY_INV | cv::THRESH_OTSU);
  yield("Otsu", pp);
  ip::erode(pp, pp, 1);
  yield("Erosion", pp);
  ip::dilate(pp, pp, 1);
  yield("Dilation", pp);

  // Boundary tracing, and cropping
  cv::Mat cropped;
  auto tracing = ip::BoundaryTracing(pp, ip::Neighborhood::Moore);
  auto inner = tracing.inner();
  yield("Inner-boundary", ip::BoundaryTracing::to_mat(inner, pp.size()));
  ip::extract_roi(inner, pp, cropped);
  yield("Cropped-ROI", cropped);

  cv::Mat output = cropped;

  // Feature extraction
  ip::Coord border_center = ml::center_of_mass(inner);
  double radius = ml::radius(inner, border_center);

  ip::PCA pca(inner);
  cv::Mat axes;
  pca.draw_axes(pp, axes, radius);
  yield("PCA-Axes", axes, true);

  cv::Mat roi_mask;
  pp.convertTo(roi_mask, CV_8UC1);
  roi_mask /= 255;
  cv::Mat texture = input / (255 / kGrayLevels);

  // LBP
  ml::LBP lbp(texture, roi_mask);
  std::ofstream lbp_file{kOutputDir + "lbp.tab"};
  for (auto v : lbp.histogram()) lbp_file << v << "\t";
  lbp_file << "\n";
  lbp_file.close();

  ml::Symmetry symmetry(cropped);

  double area = cv::countNonZero(output);
  double diameter = ml::diameter(inner);
  double avg_dist = ml::average_distance(inner, border_center);
  double perimeter = static_cast<double>(inner.size());
  ip::Coord_<double> center_of_mass = ml::center_of_mass(cropped);

  std::ofstream tex{kOutputDir + "features.tex"};
  tex << "\\begin{tabular}{ l r }\n";
  tex << "\\toprule\n";

  tex << R"(\bf Característica & \bf Valor \\ \midrule)"
         "\n";

  const std::string eol{R"(\\)"
                        "\n"};
  const std::string tab{R"(\hspace{10pt})"};

  tex << "Perímetro & " << perimeter << "\\hphantom{.00}" << eol;
  tex << "Área & " << area << "\\hphantom{.00}" << eol;
  tex << std::fixed << std::setprecision(2);
  tex << "Raio & " << radius << eol;
  tex << "Diâmetro & " << diameter << eol;
  tex << "Distância à borda & " << avg_dist << eol;

  tex << "Centro & (" << center_of_mass.i / cropped.rows;
  tex << ", " << center_of_mass.j / cropped.cols << ")" << eol;

  tex << R"(\multicolumn{2}{l}{\bf Simetria})" << eol;
  tex << tab << "Horizontal & " << symmetry.horizontal() << eol;
  tex << tab << "Vertical & " << symmetry.vertical() << eol;

  auto rectangle = ip::extract_rectangle(inner);

  auto pcai = static_cast<double>(pca.center().i - rectangle.y) / cropped.rows;
  auto pcaj = static_cast<double>(pca.center().j - rectangle.x) / cropped.cols;
  tex << R"(\multicolumn{2}{l}{\bf Eixos})" << eol;
  tex << tab << "Centro & " << ip::Coord_<double>(pcai, pcaj) << eol;
  std::string eigen1{R"(\lambda_1 \cdot \vec{v}_1)"};
  std::string eigen2{R"(\lambda_2 \cdot \vec{v}_2)"};
  tex << tab << "$|" << eigen1 << "|$ &" << pca.magnitude(0) << eol;
  tex << tab << "$|" << eigen2 << "|$ &" << pca.magnitude(1) << eol;
  tex << tab << "$\\theta(" << eigen1 << ")$ & ";
  tex << std::setprecision(2) << pca.angle() << std::setprecision(0);
  tex << " (" << pca.angle() * 180 / M_PI << "\\textdegree)" << eol;
  tex << tab << "$\\theta(" << eigen1 << "," << eigen2 << ")$ & ";
  auto angle_between = pca.axis(0).angle(pca.axis(1));
  tex << std::setprecision(2) << angle_between << std::setprecision(0);
  tex << " (" << angle_between * 180 / M_PI << "\\textdegree)" << eol;

  auto curvature = ml::curvature(inner, kSigma);
  double curv_bending_energy = ml::bending_energy(curvature);
  double curv_number_of_peaks = ml::number_of_peaks(curvature, kSigma);

  auto signature = ml::signature(inner, border_center);
  double sig_number_of_peaks = ml::number_of_peaks(signature, kSigma);

  ml::RingProjection ring_projection(output, center_of_mass, radius);
  auto ring_signature = ring_projection.distribution();
  double ring_sig_number_of_peaks = ml::number_of_peaks(ring_signature, kSigma);

  tex << R"(\multicolumn{2}{l}{\bf Picos})" << eol;
  tex << tab << "Assinatura & " << sig_number_of_peaks << eol;
  tex << tab << "Curvatura & " << curv_number_of_peaks << eol;
  tex << tab << "Projeção anelar & " << ring_sig_number_of_peaks << eol;

  tex << R"(\multicolumn{2}{l}{\bf \itshape Bending energy})" << eol;
  tex << std::fixed << std::setprecision(2);
  tex << tab << "{Curvatura} & " << curv_bending_energy << eol;

  tex << "\\bottomrule\n";
  tex << "\\end{tabular}\n";
  tex.close();

  tex.open(kOutputDir + "fourier.tex");
  tex << "\\begin{tabular}{l";
  for (std::size_t i = 0; i < kNumberOfFourierDescriptors; ++i) tex << "c";
  tex << "}\n";
  tex << R"(\bf Informação & \multicolumn{)" << kNumberOfFourierDescriptors;
  tex << R"(}{l}{\bf Descritores de Fourier} \\ \midrule)"
         "\n";
  for (std::size_t i = 0; i < kNumberOfFourierDescriptors; ++i)
    tex << "& \\footnotesize " << i + 1;
  tex << eol;

  {
    std::ofstream inner_file{(kOutputDir + "inner.tab").c_str()};
    std::vector<int> inner_i, inner_j;
    std::transform(inner.begin(), inner.end(), std::back_inserter(inner_i),
                   [](ip::Coord x) { return x.i; });
    std::transform(inner.begin(), inner.end(), std::back_inserter(inner_j),
                   [](ip::Coord x) { return x.j; });
    yield_tab(inner_file, inner_i);
    yield_tab(inner_file, inner_j);

    auto to_real = [](ip::Complex x) -> double { return x.real(); };
    auto to_complex =
        [](double x) -> ip::Complex { return static_cast<float>(x); };

    // Curvature
    std::ofstream curv_file{(kOutputDir + "curv.tab").c_str()};
    auto ccurv = convert(curvature, to_complex);
    yield_tab(curv_file, convert(ip::gaussian(ccurv, kSigma), to_real));
    yield_tab(curv_file, convert(ip::derivative(ccurv, kSigma), to_real));
    yield_tab(curv_file, convert(ip::derivative(ccurv, kSigma, 2.0), to_real));

    // Signature
    std::ofstream sig_file{(kOutputDir + "sig.tab").c_str()};
    auto csig = convert(signature, to_complex);
    yield_tab(sig_file, convert(ip::gaussian(csig, kSigma), to_real));
    yield_tab(sig_file, convert(ip::derivative(csig, kSigma), to_real));
    yield_tab(sig_file, convert(ip::derivative(csig, kSigma, 2.0), to_real));

    // Ring signature
    std::ofstream ring_sig_file{(kOutputDir + "ring_sig.tab").c_str()};
    auto cring_sig = convert(ring_signature, to_complex);
    yield_tab(ring_sig_file, convert(ip::gaussian(cring_sig, kSigma), to_real));
    yield_tab(ring_sig_file, convert(ip::derivative(cring_sig, kSigma), to_real));
    yield_tab(ring_sig_file, convert(ip::derivative(cring_sig, kSigma, 2.0), to_real));
  }

  auto inner_fourier =
      ml::fourier_descriptor(inner, kNumberOfFourierDescriptors);
  auto curv_fourier =
      ml::fourier_descriptor(curvature, kNumberOfFourierDescriptors);
  auto sig_fourier =
      ml::fourier_descriptor(signature, kNumberOfFourierDescriptors);
  auto ring_sig_fourier =
      ml::fourier_descriptor(ring_signature, kNumberOfFourierDescriptors);

  tex << "Contorno &";
  yield_normalized(tex, inner_fourier);
  tex << "Curvatura &";
  yield_normalized(tex, curv_fourier);
  tex << "Assinatura &";
  yield_normalized(tex, sig_fourier);
  tex << "Projeção anelar &";
  yield_normalized(tex, ring_sig_fourier);

  tex << "\\bottomrule\n";
  tex << "\\end{tabular}\n";
  tex.close();

  std::cout << "Running R script...\n";
  std::system("/usr/bin/R --slave -f ./process_example.R");
  return 0;
}
