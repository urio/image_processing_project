// Copyright © 2015, Filipe Verri and Paulo Urio.
#ifndef WINDOW_H_
#define WINDOW_H_

#include <opencv2/core/core.hpp>

#include <string>
#include <vector>

#include "./coord.h"

namespace ip {

enum {
  LOG_SCALE = 2,  // Convert to log-scale
  SHIFT = 4,      // Shift quadrants
  DETACHED = 8    // Do not destroy window within object destructor
};

enum { KEY_ENTER = 10, KEY_ENTER_WIN = 13, KEY_NUMPAD_ENTER = -115 };

class Window {
 public:
  // Wait for any of |keys| to be pressed. An empty list means any key.
  static int wait(const std::vector<int>& keys = {});

  Window(std::string win_title, const cv::Mat& src, int flags = 0);

  // Accept a list |src| of images. Paste all images side-by-side and only use
  // |flags| after pasting all images.
  Window(std::string win_title, const std::vector<cv::Mat>& src, int flags = 0);

  Window(std::string win_title, const std::vector<Coord>& boundary, cv::Size sz,
         int flags = 0);

  ~Window();

  void show() const;

  const std::string& title() const { return title_; }

 private:
  void prepare(const std::vector<cv::Mat>& src);

  void prepare_image(cv::InputArray src, cv::OutputArray dst);

  void setup_window();

  void set_detached(bool value) { detached_ = value; }

  bool detached() const { return detached_; }

  bool detached_;
  const std::string title_;
  int flags_;
  cv::Mat image_;
};

}  // namespace ip

#endif  // WINDOW_H_
