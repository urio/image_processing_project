// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:
#ifndef COORD_H_
#define COORD_H_

#include <opencv2/core/core.hpp>

#include <cmath>

#include <typeindex>
#include <typeinfo>
#include <ostream>
#include <type_traits>
#include <utility>

#include "./util.h"

namespace ip {

template <class T>
struct Coord_ {
  Coord_() : i{}, j{} {}

  Coord_(const Coord_&) = default;
  Coord_(Coord_&&) = default;

  explicit Coord_(const cv::Point& p) : i(p.y), j(p.x) {}

  Coord_(T i, T j) : i(i), j(j) {}

  ~Coord_() = default;

  Coord_& operator=(const Coord_&) = default;
  Coord_& operator=(Coord_&&) = default;

  Coord_& operator+=(const Coord_& b) {
    i += b.i;
    j += b.j;
    return *this;
  }

  Coord_& operator+=(T scalar) {
    i += scalar;
    j += scalar;
    return *this;
  }

  Coord_ operator+(const Coord_& b) const { return {i + b.i, j + b.j}; }

  Coord_ operator+(T scalar) const { return {i + scalar, j + scalar}; }

  Coord_ operator-(const Coord_& b) const { return {i - b.i, j - b.j}; }

  Coord_ operator-(T scalar) const { return {i - scalar, j - scalar}; }

  Coord_ operator/(T scalar) const { return {i / scalar, j / scalar}; }

  Coord_ operator/(cv::Size size) const { return (*this) / size.area(); }

  Coord_ operator*(T scalar) const { return {i * scalar, j * scalar}; }

  template <class U, typename CT = typename std::common_type<T, U>::type>
  bool operator==(const Coord_<U>& b) const {
    return util::almost_equal<CT>(i, b.i) && util::almost_equal<CT>(j, b.j);
  }

  template <class U, typename CT = typename std::common_type<T, U>::type>
  bool operator!=(const Coord_<U>& b) const {
    return !this->operator==<U, CT>(b);
  }

  operator cv::Point_<T>() const { return cv::Point_<T>(j, i); }

  template <class U, typename X = typename std::enable_if<
                         std::is_convertible<T, U>::value>::type>
  operator Coord_<U>() const {
    return Coord_<U>{static_cast<U>(i), static_cast<U>(j)};
  }

  bool within(const cv::Mat& img) const {
    return i >= 0 && i < img.rows && j >= 0 && j < img.cols;
  }

  friend std::ostream& operator<<(std::ostream& os, const Coord_& a) {
    return os << "(" << a.i << ", " << a.j << ")";
  }

  double dist(const Coord_& b) const {
    return std::hypot(static_cast<double>(i - b.i),
                      static_cast<double>(j - b.j));
  }

  double dist() const {
    return std::hypot(static_cast<double>(i), static_cast<double>(j));
  }

  double angle() const { return std::atan2(i, j); }

  double angle(const Coord_& other) const {
    auto inner = i * other.i + j * other.j;
    return std::acos(inner / (dist() * other.dist()));
  }

  T i;
  T j;
};

using Coord = Coord_<int>;

}  // namespace ip

#endif  // COORD_H_
