// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:

#include "./catch.hpp"

#include "../circle.h"
#include "../coord.h"

using Circle = ip::Circle;
using DCircle = ip::Circle_<double>;
using Coord = ip::Coord;
using DCoord = ip::Coord_<double>;

TEST_CASE("Circle", "[Circle]") {
  SECTION("Clockwise") {
    Circle c(4, 1.0);
    auto it = c.begin();
    REQUIRE(*(it + 0) == Coord(0, 1));
    REQUIRE(*(it + 1) == Coord(1, 0));
    REQUIRE(*(it + 2) == Coord(0, -1));
    REQUIRE(*(it + 3) == Coord(-1, 0));
  }
  SECTION("CounterClockwise") {
    Circle c(4, 1.0, ip::Circle::Direction::CounterClockwise);
    auto it = c.begin();
    REQUIRE(*(it + 3) == Coord(0, 1));
    REQUIRE(*(it + 2) == Coord(1, 0));
    REQUIRE(*(it + 1) == Coord(0, -1));
    REQUIRE(*(it + 0) == Coord(-1, 0));
  }
  SECTION("Clockwise centered") {
    Circle c(4, 1.0, Coord(-1, -1));
    auto it = c.begin();
    REQUIRE(*(it + 0) == Coord(-1, 0));
    REQUIRE(*(it + 1) == Coord(0, -1));
    REQUIRE(*(it + 2) == Coord(-1, -2));
    REQUIRE(*(it + 3) == Coord(-2, -1));
  }
  SECTION("Double Clockwise") {
    DCircle c(4, 1.0);
    auto it = c.begin();
    REQUIRE(*(it + 0) == DCoord(0, 1));
    REQUIRE(*(it + 1) == DCoord(1, 0));
    REQUIRE(*(it + 2) == DCoord(0, -1));
    REQUIRE(*(it + 3) == DCoord(-1, 0));
  }
  SECTION("Double CounterClockwise") {
    Circle c(4, 1.0, ip::Circle::Direction::CounterClockwise);
    auto it = c.begin();
    REQUIRE(*(it + 3) == DCoord(0, 1));
    REQUIRE(*(it + 2) == DCoord(1, 0));
    REQUIRE(*(it + 1) == DCoord(0, -1));
    REQUIRE(*(it + 0) == DCoord(-1, 0));
  }
  SECTION("Double Clockwise centered") {
    DCircle c(4, 1.0, DCoord(-1, -1));
    auto it = c.begin();
    REQUIRE(*(it + 0) == DCoord(-1, 0));
    REQUIRE(*(it + 1) == DCoord(0, -1));
    REQUIRE(*(it + 2) == DCoord(-1, -2));
    REQUIRE(*(it + 3) == DCoord(-2, -1 + -2.22045e-16));
    Coord ic(*(it + 3));
    REQUIRE(ic == Coord(-2, -1));
  }
  SECTION("Copy constructor") {
    Circle a(8, 1.0);
    Circle b(a);
    REQUIRE(a.size() == 8);
    REQUIRE(a.size() == b.size());
    Circle c = a;
    REQUIRE(a.size() == c.size());
    Circle d = Circle(8, 1.0);
    REQUIRE(d.size() == 8);
  }
}
