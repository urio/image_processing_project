// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:

#include "./catch.hpp"

#include "../coord.h"

using Coord = ip::Coord;
using DCoord = ip::Coord_<double>;

TEST_CASE("Coord", "[Coord]") {
  SECTION("Default Initialization") {
    Coord c;
    REQUIRE(c == Coord(0, 0));
    REQUIRE(c.i == 0);
    REQUIRE(c.j == 0);
  }
  SECTION("Initialization") {
    Coord c(1, 2);
    REQUIRE(c == Coord(1, 2));
    REQUIRE(c != Coord(2, 1));
    REQUIRE(c.i == 1);
    REQUIRE(c.j == 2);
  }
  SECTION("Sum") {
    Coord c;
    c += 1;
    REQUIRE(c == Coord(1, 1));
    c.j = 2;
    REQUIRE(c.i == 1);
    REQUIRE(c.j == 2);
    c = c - 1;
    REQUIRE(c == Coord(0, 1));
  }
  SECTION("Conversion") {
    Coord c(1, 2);
    cv::Point p(c);
    REQUIRE(p.x == c.j);
    REQUIRE(p.y == c.i);
    Coord c2(cv::Point(-1, 40));
    REQUIRE(c2.i == 40);
    REQUIRE(c2.j == -1);
  }
  SECTION("Scalar") {
    Coord c(10, 5);
    Coord r(c / 2);
    REQUIRE(r == Coord(5, 2));
    r = c * 2;
    REQUIRE(r == Coord(20, 10));
    r = (c * 2) / 2;
    REQUIRE(c == r);
  }
  SECTION("Double") {
    DCoord def;
    REQUIRE(def.i == Approx(0.0));
    REQUIRE(def.j == Approx(0.0));
    DCoord c(0.1, -.24);
    REQUIRE(c.i == Approx(0.1));
    REQUIRE(c.j == Approx(-0.24));
    REQUIRE(c != Coord());
    REQUIRE(Coord() != c);

    DCoord dzero(1e-100, 0.0);
    REQUIRE(dzero.i == Approx(0.0));
    REQUIRE(dzero.j == Approx(0.0));
    REQUIRE(dzero == Coord());
    REQUIRE(Coord() == dzero);
    REQUIRE(dzero == DCoord());
    REQUIRE(DCoord() == dzero);
    REQUIRE(DCoord(0.1, 0.9) != dzero);
    REQUIRE(dzero != DCoord(0.1, 0.9));

    DCoord x = DCoord(0.1, 0.9) * -3;
    REQUIRE(x == DCoord(-0.3, -2.7));
  }

}

