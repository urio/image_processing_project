// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:

#include <algorithm>
#include <vector>

#include "./catch.hpp"

#include "../datatable.h"

SCENARIO("replicate values of a vector", "[rep_len]") {
  GIVEN("an empty vector") {
    std::vector<int> v;
    WHEN("try to replicate") {
      v = ml::rep_len(v, 2);
      THEN("the result is an empty vector") {
        REQUIRE(v.empty());
      }
    }
  }
  GIVEN("a vector with a single value") {
    std::vector<int> v({2});
    WHEN("try to replicate up to five elements") {
      v = ml::rep_len(v, 5);
      THEN("all elements are the same") {
        REQUIRE(v.size() == 5);
        for (std::size_t i = 0; i < 5; ++i)
          REQUIRE(v.at(i) == 2);
      }
    }
  }
  GIVEN("a vector with 5 values") {
    std::vector<int> a({2, 4, 6, 8, 11});
    WHEN("try to replicate to 2 values") {
      std::vector<int> b(ml::rep_len(a, 2));
      THEN("the vector remains unchanged") {
        REQUIRE(a.size() == b.size());
        REQUIRE(std::equal(a.begin(), a.end(), b.begin()));
      }
    }
    WHEN("try to replicate to the same size") {
      std::vector<int> b(ml::rep_len(a, 5));
      THEN("the vector remains unchanged") {
        REQUIRE(a.size() == b.size());
        REQUIRE(std::equal(a.begin(), a.end(), b.begin()));
      }
    }
  }
  GIVEN("a vector with 3 values") {
    std::vector<int> a({2, 3, 11});
    WHEN("try to replicate to 6 values") {
      std::vector<int> b(ml::rep_len(a, 6));
      THEN("the original vector remains unchanged and a new one is created") {
        REQUIRE(a.size() == 3);
        REQUIRE(b.size() == 6);
        REQUIRE(b.at(0) == 2);
        REQUIRE(b.at(1) == 3);
        REQUIRE(b.at(2) == 11);
        REQUIRE(b.at(3) == 2);
        REQUIRE(b.at(4) == 3);
        REQUIRE(b.at(5) == 11);
      }
    }
  }
  GIVEN("a vector with 3 values") {
    std::vector<int> a({2, 3, 11});
    WHEN("try to replicate to 5 values") {
      std::vector<int> b(ml::rep_len(a, 5));
      THEN("the original vector remains unchanged and a new one is created") {
        REQUIRE(a.size() == 3);
        REQUIRE(b.size() == 5);
        REQUIRE(b.at(0) == 2);
        REQUIRE(b.at(1) == 3);
        REQUIRE(b.at(2) == 11);
        REQUIRE(b.at(3) == 2);
        REQUIRE(b.at(4) == 3);
      }
    }
  }
}

// A mesma coisa, mas mais simples
TEST_CASE("replicate values of a vector", "[rep_len]") {
  SECTION("an empty vector remains unchanged") {
    std::vector<int> v;
    v = ml::rep_len(v, 2);
    REQUIRE(v.empty());
  }
  SECTION("a vector with a single value") {
    std::vector<int> v({2});
    v = ml::rep_len(v, 5);
    REQUIRE(v.size() == 5);
    for (std::size_t i = 0; i < 5; ++i)
      REQUIRE(v.at(i) == 2);
  }
  std::vector<int> A({2, 4, 6, 8, 11});
  SECTION("replicate to a smaller size will not change") {
    std::vector<int> b(ml::rep_len(A, 2));
    REQUIRE(A.size() == b.size());
    REQUIRE(std::equal(A.begin(), A.end(), b.begin()));
  }
  SECTION("replicate to the same size will not change") {
    std::vector<int> b(ml::rep_len(A, 5));
    REQUIRE(A.size() == b.size());
    REQUIRE(std::equal(A.begin(), A.end(), b.begin()));
  }
  SECTION("replicate to a multiple size") {
    std::vector<int> a({2, 3, 11});
    std::vector<int> b(ml::rep_len(a, 6));
    REQUIRE(a.size() == 3);
    REQUIRE(b.size() == 6);
    REQUIRE(b.at(0) == 2);
    REQUIRE(b.at(1) == 3);
    REQUIRE(b.at(2) == 11);
    REQUIRE(b.at(3) == 2);
    REQUIRE(b.at(4) == 3);
    REQUIRE(b.at(5) == 11);
  }
  SECTION("replicate to a nonmultiple size") {
    std::vector<int> a({2, 3, 11});
    std::vector<int> b(ml::rep_len(a, 5));
    REQUIRE(a.size() == 3);
    REQUIRE(b.size() == 5);
    REQUIRE(b.at(0) == 2);
    REQUIRE(b.at(1) == 3);
    REQUIRE(b.at(2) == 11);
    REQUIRE(b.at(3) == 2);
    REQUIRE(b.at(4) == 3);
  }
}

