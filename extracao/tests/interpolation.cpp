// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:

#include <opencv2/core/core.hpp>

#include <array>

#include "./catch.hpp"

#include "../circle.h"
#include "../processing.h"
#include "../range.h"

using util::lang::range;
using DCircle = ip::Circle_<double>;
using DCoord = ip::Coord_<double>;
using ip::bilinear_interpolation;

TEST_CASE("Interporlation", "[interpolation]") {
  SECTION("No interpolation") {
    cv::Mat img = (cv::Mat_<float>(3, 3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);
    for (int i : range(0, 3)) {
      for (int j : range(0, 3)) {
        float x = bilinear_interpolation(img, DCoord(i, j));
        REQUIRE(x == img.at<float>(i, j));
      }
    }
  }
  SECTION("Interpolation") {
    cv::Mat img = (cv::Mat_<float>(3, 3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);
    DCircle circle(8, 1.0, DCoord(1, 1));
    std::array<double, 8> expected = {{6, 7.82843, 8, 5.57107,
                                       4, 1.67157, 2, 2.32843}};
    auto E = expected.begin();
    for (auto pt : circle) {
      float x = bilinear_interpolation(img, pt);
      REQUIRE(x == Approx(*E));
      ++E;
    }
  }
  SECTION("No interpolation with uchar") {
    cv::Mat img = (cv::Mat_<uchar>(3, 3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);
    for (int i : range(0, 3)) {
      for (int j : range(0, 3)) {
        uchar x = bilinear_interpolation<uchar>(img, DCoord(i, j));
        REQUIRE(x == img.at<uchar>(i, j));
      }
    }
  }
  SECTION("Interpolation uchar") {
    cv::Mat img = (cv::Mat_<uchar>(3, 3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);
    DCircle circle(8, 1.0, DCoord(1, 1));
    std::array<uchar, 8> expected = {{6, 8, 8, 6,
                                       4, 2, 2, 2}};
    auto E = expected.begin();
    for (auto pt : circle) {
      uchar x = bilinear_interpolation<uchar>(img, pt);
      REQUIRE(x == *E);
      ++E;
    }
  }

}

