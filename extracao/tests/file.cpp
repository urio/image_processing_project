// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:

#include "./catch.hpp"

#include "../file.h"

using File = util::File;

TEST_CASE("File class", "[File]") {
  SECTION("Normal file") {
    File f("./path/test.jpg");
    REQUIRE(f.path() == "./path");
    REQUIRE(f.basename() == "test.jpg");
    REQUIRE(f.ext() == "jpg");
  }
  SECTION("No path") {
    File f("test.jpg");
    REQUIRE(f.path() == "");
    REQUIRE(f.basename() == "test.jpg");
    REQUIRE(f.ext() == "jpg");
  }
  SECTION("Only path") {
    File f("./path/");
    REQUIRE(f.path() == "./path");
    REQUIRE(f.basename() == "");
    REQUIRE(f.ext() == "");
  }
  SECTION("No extension") {
    File f("./path/file");
    REQUIRE(f.path() == "./path");
    REQUIRE(f.basename() == "file");
    REQUIRE(f.ext() == "");
  }
  SECTION("Only extension") {
    File f(".jpg");
    REQUIRE(f.path() == "");
    REQUIRE(f.basename() == ".jpg");
    REQUIRE(f.ext() == "jpg");
  }
}

