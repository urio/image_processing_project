// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:
#ifndef DATATABLE_H_
#define DATATABLE_H_

#include <cmath>

#include <algorithm>
#include <iostream>
#include <string>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <vector>

#include "./assert.h"

namespace ml {

template <typename T>
std::vector<T> rep_len(const std::vector<T>& v, std::size_t len) {
  if (v.empty() || len <= v.size()) return v;

  std::vector<T> result{v};
  result.reserve(len);

  while (result.size() != len) {
    std::copy(v.begin(), v.begin() + static_cast<long>(std::min(
                                         len - result.size(), v.size())),
              std::back_inserter(result));
  }

  return result;
}

class TableException {};

class DataTable;
class Column;

class Cell {
 public:
  friend class DataTable;
  friend class Column;

  Cell(const Cell& source);
  Cell(Cell&& source);
  Cell(const std::string& source);
  Cell(std::string&& source);
  Cell(double source);

  ~Cell();

  Cell& operator=(const Cell& source);
  Cell& operator=(Cell&& source);
  Cell& operator=(const std::string& source);
  Cell& operator=(std::string&& source);
  Cell& operator=(double source);

  operator double&();
  operator std::string&();

  operator double() const;
  operator const std::string&() const;

  bool is(const std::type_index& type) const;
  template <typename T>
  bool is() const {
    return is(typeid(T));
  }

 private:
  Cell(const std::type_index& type);

  std::type_index type;
  union {
    std::string s;
    double d;
  };

 public:
  template <typename T>
  static constexpr bool is_valid_type() {
    return std::is_same<T, double>::value ||
           std::is_same<T, std::string>::value;
  }

  friend std::ostream& operator<<(std::ostream& os, const Cell& cell);
};

class Column {
 public:
  friend class DataTable;

  Column(const std::type_index& type_index, const std::string& name,
         const std::vector<Cell>& cells)
      : type_index{type_index}, name{name}, cells{cells} {
    for (auto&& cell : cells)
      if (!cell.is(type_index)) throw TableException{};
  }

  Column(const std::type_index& type_index, const std::string& name,
         std::vector<Cell>&& cells)
      : type_index{type_index}, name{name}, cells{std::move(cells)} {
    for (auto&& cell : cells)
      if (!cell.is(type_index)) throw TableException{};
  }

  Column(const std::type_index& type_index, const std::string& name,
         const Cell& cell)
      : type_index{type_index}, name{name}, cells{cell} {
    if (!cell.is(type_index)) throw TableException{};
  }

  Column(const std::type_index& type_index, const std::string& name)
      : type_index{type_index}, name{name} {}

  Column(const std::string& name, const Cell& cell)
      : type_index{cell.type}, name{name}, cells{cell} {}

 private:
  std::type_index type_index;
  std::string name;
  std::vector<Cell> cells;
};

class LineRange;

class LineIterator {
 public:
  friend class DataTable;
  friend class LineRange;

  LineIterator(const LineIterator&) = default;
  LineIterator(LineIterator&&) = default;

  LineIterator& operator=(const LineIterator&) = default;
  LineIterator& operator=(LineIterator&&) = default;

  LineIterator operator++(int);
  LineIterator operator++();
  LineIterator operator*();

  bool operator!=(const LineIterator& other);
  bool operator==(const LineIterator& other);

  Cell& at(std::size_t col);
  Cell& at(std::string col);

  template <typename T>
  T& at(std::size_t col) {
    return static_cast<T&>(at(col));
  }

  template <typename T>
  T& at(std::string col) {
    return static_cast<T&>(at(col));
  }

 private:
  LineIterator(std::size_t row, DataTable* table);

  std::size_t row;
  DataTable* table;
};

class LineRange {
 public:
  friend class DataTable;

  LineIterator begin();
  LineIterator end();

 private:
  LineRange(std::size_t begin, std::size_t end, DataTable* table);

  std::size_t begin_, end_;
  DataTable* table_;
};

class DataTable {
 public:
  using storage_type = std::vector<std::vector<Cell>>;
  using size_type = storage_type::size_type;

  DataTable() = default;
  ~DataTable() = default;

  DataTable(std::initializer_list<Column> columns);

  DataTable(const DataTable&) = default;
  DataTable(DataTable&&) = default;

  DataTable& operator=(const DataTable&) = default;
  DataTable& operator=(DataTable&&) = default;

  size_type ncol() const;
  size_type nrow() const;

  bool operator==(std::nullptr_t) const;
  bool operator!=(std::nullptr_t) const;

  template <typename T>
  T& at(size_type row, size_type col) {
    return static_cast<T&>(at(row, col));
  }
  template <typename T>
  const T& at(size_type row, size_type col) const {
    return static_cast<const T&>(at(row, col));
  }

  template <typename T>
  T& at(size_type row, const std::string& col) {
    return static_cast<T&>(at(row, col));
  }
  template <typename T>
  const T& at(size_type row, const std::string& col) const {
    return static_cast<const T&>(at(row, col));
  }

  template <typename T>
  T& operator()(size_type row, size_type col) {
    return static_cast<T&>(at(row, col));
  }
  template <typename T>
  const T& operator()(size_type row, size_type col) const {
    return static_cast<const T&>(at(row, col));
  }

  template <typename T>
  T& operator()(size_type row, const std::string& col) {
    return static_cast<T&>(at(row, col));
  }
  template <typename T>
  const T& operator()(size_type row, const std::string& col) const {
    return static_cast<const T&>(at(row, col));
  }

  Cell& at(size_type row, size_type col);
  const Cell& at(size_type row, size_type col) const;

  Cell& at(size_type row, const std::string& col);
  const Cell& at(size_type row, const std::string& col) const;

  Cell& operator()(size_type row, size_type col);
  const Cell& operator()(size_type row, size_type col) const;

  Cell& operator()(size_type row, const std::string& col);
  const Cell& operator()(size_type row, const std::string& col) const;

  const std::vector<std::string>& colnames() const;

  void reserve(size_type capacity);

  DataTable& rbind(std::initializer_list<Cell> row);
  DataTable& rbind(const DataTable& table);
  DataTable& rbind(DataTable&& table);

  DataTable& cbind(const Column& col);
  DataTable& cbind(Column&& col);
  DataTable& cbind(const DataTable& table);
  DataTable& cbind(DataTable&& table);

  LineRange lines();
  LineRange lines(size_type begin, size_type end);

  const DataTable& to_csv(std::ostream& os, const char* separator = ",") const;

 private:
  storage_type data_;
  size_type nrow_;

  std::unordered_map<std::string, size_type> colindexes_;
  std::vector<std::string> colnames_;

  std::vector<std::type_index> coltypes_;

 public:
  static DataTable from_csv(std::istream& is, const char* separator = ",");

  friend std::ostream& operator<<(std::ostream& os, const DataTable& table);
};

}  // namespace ml

#endif  // DATATABLE_H_
