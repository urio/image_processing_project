// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2 tw=80:
#include <opencv2/core/core.hpp>

#include <algorithm>
#include <future>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "./descriptors.h"
#include "./options.h"
#include "./processing.h"

#include "./datatable.h"

static constexpr unsigned int kMinimumNumberOfThreads = 1;
static constexpr unsigned int kGrayLevels = 64;
static constexpr double kSigma = 0.1;
static constexpr std::size_t kNumberOfFourierDescriptors = 10;

static ml::DataTable process_image(const std::string& filename,
                                   const std::string& predictive,
                                   double scale_factor = 1.0) {
  ml::DataTable table;

  // Loading and scaling
  cv::Mat input;
  ip::load(filename, input, CV_LOAD_IMAGE_GRAYSCALE, CV_8UC1);

  // Scaling
  if (!util::almost_equal(1.0, scale_factor)) {
    cv::resize(input, input, cv::Size(), scale_factor, scale_factor);
  }

  // Padding, blurring, thresholding, and eroding
  cv::Mat pp;
  cv::copyMakeBorder(input, input, 3, 3, 3, 3, cv::BORDER_CONSTANT, 0xFF);
  cv::GaussianBlur(input, pp, cv::Size(3, 3), 2);
  cv::threshold(pp, pp, 0, 255, cv::THRESH_BINARY_INV | cv::THRESH_OTSU);
  ip::erode(pp, pp, 1);
  ip::dilate(pp, pp, 1);

  // Boundary tracing, and cropping
  cv::Mat cropped;
  auto tracing = ip::BoundaryTracing(pp, ip::Neighborhood::Moore);
  auto inner = tracing.inner();
  ip::extract_roi(inner, pp, cropped);

  cv::Mat output = cropped;

  // Feature extraction
  ip::Coord border_center = ml::center_of_mass(inner);
  double radius = ml::radius(inner, border_center);

  cv::Mat roi_mask;
  pp.convertTo(roi_mask, CV_8UC1);
  roi_mask /= 255;
  cv::Mat texture = input / (255 / kGrayLevels);

  ml::LBP lbp(texture, roi_mask);

  const auto& hist = lbp.histogram();
  for (std::size_t i = 0; i < hist.size(); ++i) {
    table.cbind({"lbp " + std::to_string(i), hist[i]});
  }

  ml::Symmetry symmetry(cropped);

  double area = cv::countNonZero(output);
  double diameter = ml::diameter(inner);
  double avg_dist = ml::average_distance(inner, border_center);
  double perimeter = inner.size();
  ip::Coord_<double> center_of_mass = ml::center_of_mass(cropped);

  table.cbind({{"perimeter", perimeter},
               {"area", area},
               {"diameter", diameter},
               {"radius", radius},
               {"avg dist border", avg_dist},

               {"center i", center_of_mass.i / cropped.rows},
               {"center j", center_of_mass.j / cropped.cols},

               {"symmetry horizontal", symmetry.horizontal()},
               {"symmetry vertical", symmetry.vertical()}});

  ip::PCA pca(inner);
  auto rectangle = ip::extract_rectangle(inner);

  table.cbind(
      {{"pca center i",
        static_cast<double>(pca.center().i - rectangle.y) / cropped.rows},
       {"pca center j",
        static_cast<double>(pca.center().j - rectangle.x) / cropped.cols},

       {"axis ratio", pca.magnitude(0) / pca.magnitude(1)},
       {"axis angle", pca.angle()}});

  auto curvature = ml::curvature(inner, kSigma);
  double curv_bending_energy = ml::bending_energy(curvature);
  double curv_number_of_peaks = ml::number_of_peaks(curvature, kSigma);

  auto signature = ml::signature(inner, border_center);
  double sig_number_of_peaks = ml::number_of_peaks(signature, kSigma);

  ml::RingProjection ring_projection(output, center_of_mass, radius);
  auto ring_signature = ring_projection.distribution();
  double ring_sig_number_of_peaks = ml::number_of_peaks(ring_signature, kSigma);

  table.cbind({{"curvature bending energy", curv_bending_energy},
               {"curvature number of peaks", curv_number_of_peaks},
               {"signature number of peaks", sig_number_of_peaks},
               {"ring signature number of peaks", ring_sig_number_of_peaks}});

  auto inner_fourier =
      ml::fourier_descriptor(inner, kNumberOfFourierDescriptors);
  auto curv_fourier =
      ml::fourier_descriptor(curvature, kNumberOfFourierDescriptors);
  auto sig_fourier =
      ml::fourier_descriptor(signature, kNumberOfFourierDescriptors);
  auto ring_sig_fourier =
      ml::fourier_descriptor(ring_signature, kNumberOfFourierDescriptors);

  for (std::size_t i = 0; i < kNumberOfFourierDescriptors; ++i) {
    table.cbind({{"inner border fourier descriptor " + std::to_string(i),
                  std::abs(inner_fourier[i])},
                 {"curvature fourier descriptor " + std::to_string(i),
                  std::abs(curv_fourier[i])},
                 {"signature fourier descriptor " + std::to_string(i),
                  std::abs(sig_fourier[i])},
                 {"ring signature fourier descriptor " + std::to_string(i),
                  std::abs(ring_sig_fourier[i])}});
  }

  table.cbind({"class", predictive});

  return table;
}

static ml::DataTable process_csv_lines(ml::LineRange range) {
  ml::DataTable table;

  for (auto&& line : range) {
    auto type = line.at<std::string>("Type");
    if (type != "Scan") continue;
    auto filename = line.at<std::string>("FileName");

    std::string predictive;
    try {
      predictive = line.at<std::string>("Species");
    } catch (...) {
      predictive = "";
    }

    auto xres = line.at<double>("XRes");
    auto yres = line.at<double>("YRes");
    CV_Assert(xres == yres);
    if (util::File(filename).exists()) {
      std::cerr << "Processing " << filename << "\n";
      table.rbind(process_image(filename, predictive, 100.0 / xres));
    }
  }

  return table;
}

static ml::DataTable process_csv(const std::string& filename) {
  std::ifstream training_file{filename};
  auto training_set = ml::DataTable::from_csv(training_file);

  const auto N = training_set.nrow();
  const auto T =
      std::max(kMinimumNumberOfThreads, std::thread::hardware_concurrency());

  std::vector<std::future<ml::DataTable>> handlers;
  for (std::size_t i = 0; i < T; ++i)
    handlers.push_back(
        std::async(std::launch::async, process_csv_lines,
                   training_set.lines(i * N / T, (i + 1) * N / T)));

  ml::DataTable table;
  for (auto&& handler : handlers) {
    if (!handler.valid())
      throw std::runtime_error{"thread result is not valid"};
    table.rbind(std::move(handler.get()));
  }

  std::cerr << table.nrow() << " files were processed.\n";

  return table;
}

int main(int argc, char* argv[]) {
  options::Opts opts(argc, argv);

  ml::DataTable table;
  if (opts.input_is_image()) {
    table = process_image(opts.input, "");
  } else {
    table = process_csv(opts.input);
  }

  std::cout << table;
  return 0;
}
