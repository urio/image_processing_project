// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:
#include "./lbp.h"

#include <opencv2/core/core.hpp>

#include <cstddef>

#include <algorithm>
#include <numeric>
#include <vector>

#include <iostream>

#include "./circle.h"
#include "./processing.h"

namespace ml {

LBP::LBP(cv::InputArray src, cv::InputArray mask, ip::Circle_<double> circle,
         TextureOperator op)
    : img_(src.getMat()),
      mask_(mask.getMat()),
      circle_(circle),
      op_(op),
      histogram_(circle.size() + 2, 0.0) {
  CV_Assert(img_.type() == CV_8UC1);
  CV_Assert(op_ == TextureOperator::GRIU2I);
  run();
}

void LBP::run() {
  const int margin = static_cast<int>(circle_.radius() + 1);
  for (int i = margin; i < img_.rows - margin; i += 2 * margin) {
    for (int j = margin; j < img_.cols - margin; j += 2 * margin) {
      circle_.set_centroid(ip::Coord_<double>(i, j));
      apply_texture_operator();
    }
  }
  util::pdf(histogram_.begin(), histogram_.end(), histogram_.begin());
}

void LBP::apply_texture_operator() {
  if (mask_.at<uchar>(ip::Coord_<int>(circle_.centroid())) == 0) return;
  for (auto it = circle_.begin(); it != circle_.end(); ++it)
    if (mask_.at<uchar>(ip::Coord_<int>(*it)) == 0) return;

  const float gc = img_.at<uchar>(ip::Coord_<int>(circle_.centroid()));

  int sum = 0;
  int spatial_transitions = 0;
  int first_sign = -1;
  int last;
  for (auto it = circle_.cbegin(); it != circle_.cend(); ++it) {
    float v = ip::bilinear_interpolation<uchar>(this->img_, *it) - gc;

    int sign = (v < 0 ? 0 : 1);
    sum += sign;

    if (first_sign == -1)
      first_sign = sign;
    else if (last != sign)
      spatial_transitions++;

    if ((it + 1) == circle_.cend() && sign != first_sign) spatial_transitions++;
    last = sign;
  }
  int result = spatial_transitions <= 2 ? sum : circle_.points() + 1;
  histogram_.at(static_cast<std::size_t>(result))++;
}
}
