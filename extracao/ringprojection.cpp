// Copyright © 2015, Filipe Verri and Paulo Urio.
#include "./ringprojection.h"

#include <opencv2/core/core.hpp>

#include <cmath>

#include <algorithm>
#include <vector>

#include "./circle.h"
#include "./coord.h"
#include "./util.h"

namespace ml {

RingProjection::RingProjection(cv::InputArray src, const ip::Coord& centroid,
                                 double object_radius)
    : image_(src.getMat()), centroid_(centroid), object_radius_(object_radius) {
  CV_Assert(src.channels() == 1);
  detect();
}

void RingProjection::detect() {
  mass_distribution_.clear();
  mass_distribution_.reserve(static_cast<std::size_t>(object_radius_));
  int radius = 0;
  while (radius < object_radius_) {
    radius += 1;
    ip::Circle circle(8 * radius, radius, centroid_);
    mass_distribution_.push_back(static_cast<double>(
        std::count_if(circle.begin(), circle.end(), [&](const ip::Coord& pt) {
          return pt.within(image_) && image_.at<uchar>(pt) != 0;
        })));
  }
  util::pdf(mass_distribution_.begin(), mass_distribution_.end(),
            mass_distribution_.begin());
}

std::vector<double> RingProjection::sample() const {
  return sample(number_of_samples());
}

std::vector<double> RingProjection::sample(std::size_t samples) const {
  if (samples == mass_distribution_.size()) return mass_distribution_;

  double size = static_cast<double>(mass_distribution_.size());
  double n = static_cast<double>(samples);
  double step = size / n;
  std::vector<double> result;
  double pos = 0.0f;

  result.reserve(samples);

  // Go through the list interpolating elements
  if (samples > mass_distribution_.size()) {
    while (result.size() < samples) {
      std::size_t i = static_cast<std::size_t>(std::floor(pos));
      double a = mass_distribution_.at(i);
      double b = mass_distribution_.at(
          (i + 1) < mass_distribution_.size() ? (i + 1) : i);
      double t = pos - std::floor(pos);
      result.push_back(a * (1 - t) + b * t);
      pos += step;
    }
  }

  // Go through the list extracting the closest element
  if (samples < mass_distribution_.size()) {
    while (result.size() < samples) {
      std::size_t i = static_cast<std::size_t>(std::round(pos));
      result.push_back(mass_distribution_.at(i));
      pos += step;
    }
  }
  return result;
}

}  // namespace ml
