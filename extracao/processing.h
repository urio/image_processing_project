// Copyright © 2015, Filipe Verri and Paulo Urio.
#ifndef PROCESSING_H_
#define PROCESSING_H_

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <cmath>

#include <limits>
#include <string>
#include <type_traits>
#include <vector>
#include <valarray>

#include <iostream>

#include "./coord.h"
#include "./window.h"

namespace ip {

using Complex = std::complex<float>;

enum class Neighborhood : int { VonNeumann = 4, Moore = 8 };

void fft_padding(cv::InputArray src, cv::OutputArray dst);

void fft(cv::InputArray src, cv::OutputArray dst);
void fft(std::valarray<Complex>& x);

void ifft(cv::InputArray src, cv::OutputArray dst);
void ifft(std::valarray<Complex>& x);

// Swap 1 ←→ 3 and 2 ←→ 4 quadrants. |dst| will always have an even size.
void fftshift(cv::InputArray src, cv::OutputArray dst);

void gaussian(const std::vector<Coord>& src, double ksigma,
              std::vector<Coord>& dst);

std::valarray<Complex> gaussian(const std::valarray<Complex>& x, double sigma);
std::valarray<Complex> derivative(const std::valarray<Complex>& x,
                                  double sigma, double order = 1.0);

// Compute the magnitude and switch to logarithmic scale
// log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
void complex_magnitude(cv::InputArray src, cv::OutputArray dst);

// Convert to normalized [0, 1] log scale. dst = log(1 + |src|)
void log_scale(cv::InputArray src, cv::OutputArray dst);

// Read |filename| into |image|.
void load(const std::string& filename, cv::OutputArray image,
          int flags = CV_LOAD_IMAGE_GRAYSCALE, int type = CV_32FC1);

// Write |image| into |filename|.
bool save(const std::string& filename, cv::InputArray image);

// Combine |left| and |right| images side-by-side in |out|.
void combine(cv::InputArray left, cv::InputArray right, cv::OutputArray out);

void fill(cv::InputArray src, cv::OutputArray dst, Coord coord,
          uchar fill_color = 255);

// Get the smallest possible rectangle to fit all coordinates in |boundary|.
cv::Rect extract_rectangle(const std::vector<Coord>& boundary);

// Extract the smallest image region that contains |boundary| points.
void extract_roi(const std::vector<Coord>& boundary, cv::InputArray src,
                 cv::OutputArray dst);

// Extract the smallest |src| region that contains all |boundary| points and
// add |padding| pixels to the border with a constant |value| level.
void extract_roi(const std::vector<Coord>& boundary, cv::InputArray src,
                 cv::OutputArray dst, int padding, int value);

void erode(cv::InputArray src, cv::OutputArray dst, int erosion_size);
void dilate(cv::InputArray src, cv::OutputArray dst, int dilation_size);

cv::Mat asRowMatrix(const std::vector<cv::Mat>& src, int rtype,
                    double alpha = 1, double beta = 0);

template <class PixelT = float, class CoordT>
static inline PixelT bilinear_interpolation(const cv::Mat& img,
                                            Coord_<CoordT> pos) {
  const double x = pos.j, y = pos.i;
  const double y1 = std::floor(y - 0.5);
  const double y2 = std::floor(y + 0.5);
  const double x1 = std::floor(x - 0.5);
  const double x2 = std::floor(x + 0.5);

  const double c = 1.0 / ((x2 - x1) * (y2 - y1));
  const double w11 = (x2 - x) * (y2 - y);
  const double w12 = (x2 - x) * (y - y1);
  const double w21 = (x - x1) * (y2 - y);
  const double w22 = (x - x1) * (y - y1);

  const Coord_<int> c11(Coord_<CoordT>(y1, x1));
  const Coord_<int> c12(Coord_<CoordT>(y1, x2));
  const Coord_<int> c21(Coord_<CoordT>(y2, x1));
  const Coord_<int> c22(Coord_<CoordT>(y2, x2));

#define PIXEL(c) (c.within(img) ? static_cast<double>(img.at<PixelT>(c)) : 0.0)
  const double p11 = PIXEL(c11) * w11;
  const double p12 = PIXEL(c12) * w12;
  const double p21 = PIXEL(c21) * w21;
  const double p22 = PIXEL(c22) * w22;
  const double result = (p11 + p12 + p21 + p22) * c;
#undef PIXEL

  if (std::is_integral<PixelT>::value) {
    return static_cast<PixelT>(std::round(result));
  } else {
    return static_cast<PixelT>(result);
  }
}

}  // namespace ip

#endif  // PROCESSING_H_
