// Copyright © 2015, Filipe Verri and Paulo Urio.
//
// Tang, Y. Y., Li, B. F., Ma, H. M. H., & Lin, J. L. J. (1998).
// Ring-projection-wavelet-fractal signatures: a novel approach to feature
// extraction. IEEE Transactions on Circuits and Systems II: Analog and Digital
// Signal Processing, 45(8), 252–258. doi:10.1109/82.718824
#ifndef RINGPROJECTION_H_
#define RINGPROJECTION_H_

#include <opencv2/core/core.hpp>

#include <cstddef>

#include <vector>

#include "./circle.h"
#include "./coord.h"

namespace ml {

class RingProjection {
 public:
  RingProjection(cv::InputArray src, const ip::Coord& centroid,
                  double object_radius);

  RingProjection(const RingProjection&) = delete;
  RingProjection(RingProjection&&) = delete;
  RingProjection& operator=(const RingProjection&) = delete;
  RingProjection& operator=(RingProjection&&) = delete;

  ~RingProjection() = default;

  double score() const { return 0.0f; }

  std::vector<double> sample() const;
  std::vector<double> sample(std::size_t samples) const;

  std::size_t number_of_samples() const { return mass_distribution_.size(); }

  const std::vector<double>& distribution() const { return mass_distribution_; }

 private:
  void detect();

  cv::Mat image_;
  const ip::Coord centroid_;
  const double object_radius_;

  std::vector<double> mass_distribution_;
};

}  // namespace ml

#endif  // RINGPROJECTION_H_
