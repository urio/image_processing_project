// Copyright © 2015, Filipe Verri and Paulo Urio.
#include "./util.h"

#include <algorithm>
#include <string>

namespace util {

bool icompare(const std::string& a, const std::string& b) {
  std::string la, lb;
  std::transform(a.begin(), a.end(), std::back_inserter(la), ::tolower);
  std::transform(b.begin(), b.end(), std::back_inserter(lb), ::tolower);
  return a.compare(b) == 0;
}

}  // namespace util
