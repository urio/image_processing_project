
CXXFLAGS+=`pkg-config --cflags opencv` -Wall -Wextra -pedantic -std=c++11 \
		  -Wconversion -Wsign-conversion

LDFLAGS+=-m64 `pkg-config --libs opencv`

.PHONY: init clean tags ling cppcheck format test

OBJS = datatable.o descriptors.o file.o lbp.o pca.o processing.o \
			 ringprojection.o symmetry.o tracing.o util.o window.o

TESTS = tests/circle.o tests/coord.o tests/datatable.o tests/file.o \
				tests/interpolation.o

default: init extract

all: init test report extract

init:
	@echo "CXX: $(CXX)"
ifeq ($(RELEASE),1)
CXXFLAGS+=-O3 -march=native -DNDEBUG
else
CXXFLAGS+=-g -O0 -fstack-protector-all -Wstack-protector
endif

.depend: *.cpp *.h tests/*.cpp
	@echo -e "`tput setaf 3``tput bold`Scanning dependencies...`tput sgr0`"
	@$(shell bash ./scan_dependencies.sh)

extract: init $(OBJS) extract.o
	@echo -e "`tput setaf 39`Linking CXX executable `tput bold`$@`tput sgr0`"
	@$(CXX) $(LDFLAGS) $(OBJS) extract.o -o extract

test: init $(OBJS) tests/tests.o $(TESTS)
	@echo -e "`tput setaf 39`Linking CXX executable `tput bold`$@`tput sgr0`"
	@$(CXX) $(LDFLAGS) $(OBJS) $(TESTS) tests/tests.o -o test
	./test

report: init $(OBJS) report.o
	@echo -e "`tput setaf 39`Linking CXX executable `tput bold`$@`tput sgr0`"
	@$(CXX) $(LDFLAGS) $(OBJS) report.o -o report

%.o: %.cpp
	@echo -e "`tput setaf 2`Building CXX object `tput bold`$@`tput sgr0`"
	@$(CXX) $(CXXFLAGS) -c $< -o $@

tags:
	g++ -std=c++0x -M *.cpp *.h tests/*.cpp | sed -e 's/[\\ ]/\n/g' | \
		sed -e '/^$$/d' -e '/\.o:[ \t]*$$/d' | \
		ctags -L - --c++-kinds=+p --fields=+iaS --extra=+q

lint:
	cpplint --root=implementacao --filter=-build/c++11,-readability/function,-whitespace/operators *.cpp *.h

cppcheck:
	cppcheck --enable=all --platform=unix64 --language=c++ *.cpp *.h

format:
	clang-format -style=file -i *.cpp *.h

clean:
	$(RM) *.o ./tests/*.o ./extract ./test ./report .depend

ifeq ($(filter $(MAKECMDGOALS),tags lint cppcheck format clean),)
-include .depend
endif

