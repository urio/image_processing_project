// Copyright © 2015, Filipe Verri and Paulo Urio.
#ifndef CIRCLE_H_
#define CIRCLE_H_

#define _USE_MATH_DEFINES
#include <cmath>

#include <cstddef>

#include <algorithm>
#include <iterator>
#include <type_traits>
#include <vector>

#include "./coord.h"

namespace ip {

template <class T>
class Circle_ {
 public:
  enum class Direction : int { Clockwise = 1, CounterClockwise = 2 };

  typedef typename std::vector<Coord_<T>>::const_iterator const_iterator;

  const_iterator begin() const { return circle_.begin(); }
  const_iterator cbegin() const { return circle_.cbegin(); }
  const_iterator end() const { return circle_.end(); }
  const_iterator cend() const { return circle_.cend(); }

  Coord_<T> at(std::size_t index) const { return circle_.at(index); }

  Circle_() : Circle_{4, 1.0} {}

  Circle_(int points, double radius, Direction dir = Direction::Clockwise);

  Circle_(int points, double radius, Coord_<T> centroid,
          Direction dir = Direction::Clockwise);

  Circle_(const Circle_&) = default;

  Circle_(Circle_&&) = default;

  ~Circle_() = default;

  Circle_& operator=(const Circle_&) = delete;
  Circle_& operator=(Circle_&&) = delete;

  std::size_t size() const { return circle_.size(); }

  int points() const { return points_; }

  double radius() const { return radius_; }

  Coord_<T> centroid() const { return centroid_; }

  void set_centroid(Coord_<T> new_centroid);

 private:
  // Generate the circle’s points
  void update();

  Coord_<T> centroid_;
  int points_;
  double radius_;
  Direction dir_;
  std::vector<Coord_<T>> circle_;
};

using Circle = Circle_<int>;

template <class T>
Circle_<T>::Circle_(int points, double radius, Direction dir)
    : Circle_{points, radius, Coord_<T>{{}, {}}, dir} {
  CV_Assert(points > 0);
  CV_Assert(radius >= 1.0f);
}

template <class T>
Circle_<T>::Circle_(int points, double radius, Coord_<T> centroid,
                    Direction dir)
    : centroid_(centroid), points_(points), radius_(radius), dir_(dir) {
  update();
}

template <class T>
void Circle_<T>::update() {
  const double c = 2.0 * M_PI / points_;
  circle_.clear();
  for (int i = 0; i < points_; ++i) {
    double x, y;
    if (std::is_integral<T>::value) {
      y = std::round(radius_ * std::sin(c * i));
      x = std::round(radius_ * std::cos(c * i));
    } else {
      y = radius_ * std::sin(c * i);
      x = radius_ * std::cos(c * i);
    }
    Coord_<T> circle_pt(static_cast<T>(y), static_cast<T>(x));
    circle_.push_back(centroid_ + circle_pt);
  }
  if (dir_ == Direction::CounterClockwise)
    std::reverse(circle_.begin(), circle_.end());
}

template <class T>
void Circle_<T>::set_centroid(Coord_<T> new_centroid) {
  if (centroid_ == new_centroid) return;
  centroid_ = new_centroid;
  update();
}

}  // namespace ip

#endif  // CIRCLE_H_
