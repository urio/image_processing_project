// Copyright © 2015, Filipe Verri and Paulo Urio.
#ifndef DESCRIPTORS_H_
#define DESCRIPTORS_H_

#include <vector>

#include "./coord.h"
#include "./lbp.h"
#include "./pca.h"
#include "./processing.h"
#include "./ringprojection.h"
#include "./symmetry.h"
#include "./tracing.h"

namespace ml {

double diameter(const std::vector<ip::Coord>& boundary);

double radius(const std::vector<ip::Coord>& boundary,
              const ip::Coord& centroid);

// Assuming it’s a convex object?
double average_distance(const std::vector<ip::Coord>& boundary,
                        const ip::Coord& centroid);

ip::Coord center_of_mass(const std::vector<ip::Coord>& boundary);
ip::Coord center_of_mass(const cv::Mat& src);

std::vector<ip::Complex> fourier_descriptor(
    const std::vector<ip::Coord>& boundary);
std::vector<ip::Complex> fourier_descriptor(
    const std::vector<ip::Coord>& boundary, std::size_t M);

std::vector<double> fourier_descriptor(const std::vector<double>& x);
std::vector<double> fourier_descriptor(const std::vector<double>& x,
                                       std::size_t M);

std::vector<double> signature(const std::vector<ip::Coord>& boundary);
std::vector<double> signature(const std::vector<ip::Coord>& boundary,
                              ip::Coord cm);

std::vector<double> curvature(const std::vector<ip::Coord>& boundary,
                              double sigma);

double bending_energy(const std::vector<double>& x);
int number_of_peaks(const std::vector<double>& x, double sigma);

}  // namespace ml

#endif  // DESCRIPTORS_H_
