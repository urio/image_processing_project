// Copyright © 2015, Filipe Verri and Paulo Urio.
#include "./tracing.h"

#include <opencv2/core/core.hpp>

#include <array>
#include <vector>

#include <iostream>

#include "./range.h"

namespace ip {

BoundaryTracing::BoundaryTracing(cv::InputArray src, Neighborhood nb)
    : img_(src.getMat()) {
  CV_Assert(src.type() == CV_8UC1);
  init_neighborhood(nb);
  run();
}

void BoundaryTracing::init_neighborhood(Neighborhood nb) {
  const std::array<Coord, 4> four = {{
      {+0, +1},  // 0
      {-1, +0},  // 1
      {+0, -1},  // 2
      {+1, +0},  // 3
  }};
  const std::array<Coord, 8> eight = {{
      {+0, +1},  // 0
      {-1, +1},  // 1
      {-1, +0},  // 2
      {-1, -1},  // 3
      {+0, -1},  // 4
      {+1, -1},  // 5
      {+1, +0},  // 6
      {+1, +1}   // 7
  }};
  if (nb == Neighborhood::VonNeumann) {
    even_offset_ = odd_offset_ = 3;
    dir_ = 0;
    nb_.insert(std::end(nb_), std::begin(four), std::end(four));
  } else if (nb == Neighborhood::Moore) {
    even_offset_ = 7;
    odd_offset_ = 6;
    dir_ = 7;
    nb_.insert(std::end(nb_), std::begin(eight), std::end(eight));
  } else {
    CV_Error(0, "Unsupported neighborhood");
  }
  nb_size_ = static_cast<int>(nb_.size());
}

void BoundaryTracing::run() {
  find_starting_point();
  next();
  Coord end = *(boundary_.begin() + 1);
  do {
    next();
  } while (end != pt_);
}

void BoundaryTracing::next() {
  using util::lang::range;

  unsigned char value = img_.at<unsigned char>(pt_);
  int start =
      (dir_ + ((dir_ % 2 == 1) ? odd_offset_ : even_offset_)) % nb_size_;
  for (int i : range(0, nb_size_)) {
    int d = (start + i) % nb_size_;
    Coord neighbor(pt_ + nb_.at(static_cast<size_t>(d)));
    if (!neighbor.within(img_)) continue;
    if (value == img_.at<unsigned char>(neighbor)) {
      dir_ = d;
      update_current_point(neighbor);
      return;
    }
  }
  CV_Error(0, "WTF?");
}

void BoundaryTracing::find_starting_point() {
  using util::lang::range;

  for (int i : range(0, img_.rows))
    for (int j : range(0, img_.cols)) {
      if (img_.at<unsigned char>(i, j) != 0x0) {
        if (img_.at<unsigned char>(i + 1, j + 1) != 0x0) {
          update_current_point(Coord(i, j));
          return;
        }
      }
    }
  CV_Error(-1, "All pixels are zero.");
}

void BoundaryTracing::update_current_point(const Coord& pt) {
  // std::cerr << "Coord" << pt << std::endl;
  boundary_.push_back(pt);
  pt_ = pt;
}

const std::vector<Coord> BoundaryTracing::inner() const {
  CV_Assert(boundary_.size() > 2);
  std::vector<Coord> boundary(boundary_.begin(), boundary_.end() - 2);
  return boundary;
}

const std::vector<Coord> BoundaryTracing::outer() const {
  CV_Error(0, "Not implemented");
  return std::vector<Coord>{};
}

cv::Mat BoundaryTracing::to_mat(const std::vector<Coord>& boundary,
                                cv::Size sz) {
  cv::Mat result = cv::Mat::zeros(sz, CV_8UC1);
  for (Coord c : boundary) {
    result.at<uchar>(c) = 0xff;
  }
  return result;
}

}  // namespace ip
