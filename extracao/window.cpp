// Copyright © 2015, Filipe Verri and Paulo Urio.
#include "./window.h"

#include <opencv2/highgui/highgui.hpp>

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>

#include "./processing.h"
#include "./tracing.h"

namespace ip {

int Window::wait(const std::vector<int>& keys) {
  int k;
  while ((k = cv::waitKey(0))) {
    k = static_cast<int>(static_cast<char>(k));
    if (keys.empty() || std::any_of(std::begin(keys), std::end(keys),
                                    [k](int i) { return k == i; }))
      break;
  }
  return k;
}

Window::Window(std::string win_title, const cv::Mat& src, int flags)
    : Window(win_title, std::vector<cv::Mat>{src}, flags) {}

Window::Window(std::string win_title, const std::vector<cv::Mat>& src,
               int flags)
    : title_(win_title), flags_(flags) {
  prepare(src);
  setup_window();
  show();
  set_detached(flags & DETACHED);
}

Window::Window(std::string win_title, const std::vector<Coord>& boundary,
               cv::Size sz, int flags)
    : title_(win_title), flags_(flags) {
  CV_Assert(!(flags & ~DETACHED));
  image_ = BoundaryTracing::to_mat(boundary, sz);
  setup_window();
  show();
  set_detached(flags & DETACHED);
}

Window::~Window() {
  if (!detached()) cv::destroyWindow(title());
}

void Window::show() const {
  cv::imshow(title(), image_);
  cv::resizeWindow(title(), image_.cols, image_.rows);
}

void Window::prepare(const std::vector<cv::Mat>& src) {
  CV_Assert(!src.empty());
  // Paste each image side-by-side.
  prepare_image(*src.begin(), image_);
  if (src.size() == 1) return;
  cv::Mat img;
  for (auto it = src.begin() + 1; it != src.end(); ++it) {
    prepare_image(*it, img);
    ip::combine(image_, img, image_);
  }
}

void Window::prepare_image(cv::InputArray src, cv::OutputArray dst) {
  cv::Mat img = src.getMat().clone();
  // Assume a two-channel matrix to be a complex matrix.
  if (img.type() == CV_32FC2) complex_magnitude(img, img);
  // Apply user flags.
  if (flags_ & LOG_SCALE) log_scale(img, img);
  if (flags_ & SHIFT) fftshift(img, img);
  // Normalize complex and/or grayscale images
  if (img.type() == CV_32FC1) cv::normalize(img, img, 0, 1, CV_MINMAX);
  img.copyTo(dst);
}

void Window::setup_window() { cv::namedWindow(title(), cv::WINDOW_NORMAL); }

}  // namespace ip
