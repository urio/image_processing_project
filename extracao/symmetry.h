// Copyright © 2015, Filipe Verri and Paulo Urio.
//
// This implementation assumes the axes of the object are exactly in the middle
// of the image.
#ifndef SYMMETRY_H_
#define SYMMETRY_H_

#include <opencv2/core/core.hpp>

namespace ml {

class Symmetry {
 public:
  explicit Symmetry(cv::InputArray src) : image_(src.getMat()) {
    CV_Assert(src.type() == CV_8UC1);
  }

  ~Symmetry() = default;

  double vertical() const;

  double horizontal() const;

  double maximum() const;

 private:
  cv::Mat image_;
};

}  // namespace ml

#endif  // SYMMETRY_H_
