// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:
#include "./descriptors.h"

#include <opencv2/core/core.hpp>

#include <algorithm>
#include <numeric>
#include <utility>
#include <vector>

#include "./range.h"

#include "./coord.h"
#include "./util.h"

namespace ml {

double diameter(const std::vector<ip::Coord>& boundary) {
  auto pairs = util::unique_combinations(boundary.begin(), boundary.end());
  return std::accumulate(
      pairs.begin(), pairs.end(), 0.0f,
      [](double current, const std::pair<ip::Coord, ip::Coord>& pair) {
        return std::max(current, pair.first.dist(pair.second));
      });
}

double radius(const std::vector<ip::Coord>& boundary,
              const ip::Coord& centroid) {
  return std::accumulate(boundary.begin(), boundary.end(), 0.0f,
                         [&centroid](double current, const ip::Coord& coord) {
                           return std::max(current, centroid.dist(coord));
                         });
}

double average_distance(const std::vector<ip::Coord>& boundary,
                        const ip::Coord& centroid) {
  double sum = std::accumulate(boundary.begin(), boundary.end(), 0.0f,
                               [&centroid](double sum, const ip::Coord& coord) {
                                 return sum + centroid.dist(coord);
                               });
  return sum / boundary.size();
}

ip::Coord center_of_mass(const std::vector<ip::Coord>& boundary) {
  return std::accumulate(boundary.begin(), boundary.end(), ip::Coord()) /
         static_cast<int>(boundary.size());
}

ip::Coord center_of_mass(const cv::Mat& img) {
  CV_Assert(img.type() == CV_8UC1);

  double di = 0.0, dj = 0.0;
  double mass = 0;

  for (int i = 0; i < img.rows; ++i) {
    for (int j = 0; j < img.cols; ++j) {
      auto val = static_cast<double>(img.at<uchar>(i, j));

      mass += val;
      di += i * val;
      dj += j * val;
    }
  }

  return {static_cast<int>(di / mass), static_cast<int>(dj / mass)};
}

std::vector<ip::Complex> fourier_descriptor(
    const std::vector<ip::Coord>& boundary) {
  return fourier_descriptor(boundary, boundary.size());
}

std::vector<ip::Complex> fourier_descriptor(
    const std::vector<ip::Coord>& boundary, std::size_t M) {
  std::valarray<ip::Complex> tmp(boundary.size());

  std::transform(
      boundary.begin(), boundary.end(), std::begin(tmp),
      [](const ip::Coord& coord) { return ip::Complex(coord.i, coord.j); });

  ip::fft(tmp);

  return {std::begin(tmp), std::begin(tmp) + M};
}

std::vector<double> fourier_descriptor(const std::vector<double>& x) {
  return fourier_descriptor(x, x.size());
}

std::vector<double> fourier_descriptor(const std::vector<double>& x,
                                       std::size_t M) {
  std::valarray<ip::Complex> tmp(x.size());

  std::transform(x.begin(), x.end(), std::begin(tmp), [](double v) {
    return ip::Complex{static_cast<float>(v), 0.0f};
  });

  ip::fft(tmp);

  std::vector<double> result;
  result.reserve(M);
  std::transform(std::begin(tmp), std::begin(tmp) + M,
                 std::back_inserter(result),
                 [](ip::Complex v) -> double { return std::abs(v); });

  return result;
}

std::vector<double> signature(const std::vector<ip::Coord>& boundary) {
  return signature(boundary, center_of_mass(boundary));
}

std::vector<double> signature(const std::vector<ip::Coord>& boundary,
                              ip::Coord cm) {
  std::vector<double> result;
  result.reserve(boundary.size());

  for (auto&& coord : boundary) result.push_back(coord.dist(cm));

  return result;
}

std::vector<double> curvature(const std::vector<ip::Coord>& boundary,
                              double sigma) {
  std::vector<double> result;
  result.reserve(boundary.size());
  std::valarray<ip::Complex> x(boundary.size()), y(boundary.size());

  std::size_t i = 0;
  for (auto&& coord : boundary) {
    x[i] = coord.j;
    y[i] = coord.i;
    ++i;
  }

  const auto dx = ip::derivative(x, sigma);
  const auto dy = ip::derivative(y, sigma);

  const auto d2x = ip::derivative(x, sigma, 2.0);
  const auto d2y = ip::derivative(y, sigma, 2.0);

  for (i = 0; i < boundary.size(); ++i)
    result.push_back(
        (dx[i].real() * d2y[i].real() - dy[i].real() * d2x[i].real()) /
        std::pow(dx[i].real() * dx[i].real() + dy[i].real() * dy[i].real(),
                 3.0 / 2.0));

  for (auto&& value : result)
    if (std::isnan(value)) value = 0.0;

  return result;
}

double bending_energy(const std::vector<double>& x) {
  return std::accumulate(x.begin(), x.end(), 0.0,
                         [](double acc, double v) { return acc + v * v; });
}

int number_of_peaks(const std::vector<double>& x, double sigma) {
  std::valarray<ip::Complex> d2x(x.size());
  std::copy(x.begin(), x.end(), std::begin(d2x));

  d2x = ip::derivative(d2x, sigma, 2.0);

  auto sign = [](double x) {
    return util::almost_equal(1.0 + x, 1.0) ? 0 : (x < 0 ? -1 : 1);
  };

  int peaks = 0, last_sign = sign(d2x[0].real());
  for (auto&& value : d2x) {
    int new_sign = sign(value.real());
    if (new_sign != 0 && new_sign != last_sign) {
      last_sign = new_sign;
      ++peaks;
    }
  }

  return peaks;
}

}  // namespace ml
