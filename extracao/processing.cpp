// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:
#include "./processing.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <cstddef>
#include <cstdlib>

#include <algorithm>
#include <complex>
#include <iostream>
#include <limits>
#include <utility>
#include <string>
#include <vector>
#include <stack>

#include "./range.h"

#include "./coord.h"
#include "./util.h"

namespace ip {

const float kPi = static_cast<float>(M_PI);

void fft_padding(cv::InputArray src, cv::OutputArray dst) {
  cv::Mat image = src.getMat();
  cv::Mat padded;
  int m = cv::getOptimalDFTSize(image.rows);
  int n = cv::getOptimalDFTSize(image.cols);

  cv::copyMakeBorder(image, padded, 0, m - image.rows, 0, n - image.cols,
                     cv::BORDER_CONSTANT, cv::Scalar::all(0));
  padded.copyTo(dst);
}

void fft(cv::InputArray src, cv::OutputArray dst) {
  cv::Mat input = src.getMat();
  cv::Mat padded;

  CV_Assert(src.size().height > 1);

  fft_padding(input, padded);

  // planes[0] is the real part, planes[1] is the imaginary part.
  std::vector<cv::Mat> planes{cv::Mat_<float>(padded),
                              cv::Mat::zeros(padded.size(), CV_32F)};
  cv::Mat complex;
  cv::merge(planes, complex);

  cv::dft(complex, complex, cv::DFT_COMPLEX_OUTPUT);
  complex.copyTo(dst);
}

void fft(std::valarray<std::complex<float>>& x) {
  const auto N = x.size();
  if (N <= 1) return;

  std::valarray<std::complex<float>> even = x[std::slice(0, N / 2, 2)];
  std::valarray<std::complex<float>> odd = x[std::slice(1, N / 2, 2)];

  fft(even);
  fft(odd);

  for (size_t k = 0; k < N / 2; ++k) {
    auto t = std::polar(1.0f, -2 * kPi * k / N) * odd[k];
    x[k] = even[k] + t;
    x[k + N / 2] = even[k] - t;
  }
}

void ifft(cv::InputArray src, cv::OutputArray dst) {
  cv::Mat input = src.getMat();
  cv::idft(input, dst, cv::DFT_REAL_OUTPUT | cv::DFT_SCALE);
}

void ifft(std::valarray<std::complex<float>>& x) {
  x = x.apply(std::conj);
  fft(x);
  x = x.apply(std::conj);

  x /= x.size();
}

void fftshift1d(cv::InputArray src, cv::OutputArray dst) {
  CV_Assert(src.size().width == 1 && src.size().height > 1);
  cv::Mat magI = src.getMat();

  // Crop the spectrum, if it has an odd number of rows or columns
  magI = magI(cv::Rect(0, 0, 1, magI.rows & -2));

  // Rearrange the quadrants of Fourier image  so that the origin is at the
  // image center
  int cy = magI.rows / 2;

  cv::Mat q0(magI, cv::Rect(0, 0, 1, cy));   // Top
  cv::Mat q1(magI, cv::Rect(0, cy, 1, cy));  // Bottom

  cv::Mat tmp;  // Swap top with bottom
  q0.copyTo(tmp);
  q1.copyTo(q0);
  tmp.copyTo(q1);

  magI.copyTo(dst);
}

void fftshift(cv::InputArray src, cv::OutputArray dst) {
  if (src.size().width == 1) {
    fftshift1d(src, dst);
    return;
  }

  cv::Mat magI = src.getMat();

  // Crop the spectrum, if it has an odd number of rows or columns
  magI = magI(cv::Rect(0, 0, magI.cols & -2, magI.rows & -2));

  // Rearrange the quadrants of Fourier image  so that the origin is at the
  // image center
  int cx = magI.cols / 2;
  int cy = magI.rows / 2;

  cv::Mat q0(magI, cv::Rect(0, 0, cx, cy));    // Top-Left
  cv::Mat q1(magI, cv::Rect(cx, 0, cx, cy));   // Top-Right
  cv::Mat q2(magI, cv::Rect(0, cy, cx, cy));   // Bottom-Left
  cv::Mat q3(magI, cv::Rect(cx, cy, cx, cy));  // Bottom-Right

  cv::Mat tmp;  // Swap quadrants (Top-Left with Bottom-Right)
  q0.copyTo(tmp);
  q3.copyTo(q0);
  tmp.copyTo(q3);

  q1.copyTo(tmp);  // Swap quadrant (Top-Right with Bottom-Left)
  q2.copyTo(q1);
  tmp.copyTo(q2);

  magI.copyTo(dst);
}

static std::size_t next_power_2(std::size_t i) {
  std::size_t j = 2;
  while (j < i) j *= 2;
  return j;
}

std::valarray<Complex> gaussian(const std::valarray<Complex>& x, double sigma) {
  std::valarray<Complex> result(next_power_2(x.size()));
  for (std::size_t i = 0; i < result.size(); ++i)
    result[i] = x[i % x.size()];

  sigma = 1.0f / sigma;
  auto s = static_cast<float>(sigma * sigma);

  fft(result);
  for (std::size_t f = 0; f < result.size(); ++f)
    result[f] *= std::exp(-0.5f * f * f / s);
  ifft(result);

  return result.apply([](Complex c) -> Complex { return c.real(); })[std::slice(0, x.size(), 1)];
}

std::valarray<Complex> derivative(const std::valarray<Complex>& x,
                                  double sigma, double order) {
  std::valarray<Complex> result(0.0f, next_power_2(x.size()));
  for (std::size_t i = 0; i < result.size(); ++i)
    result[i] = x[i % x.size()];

  sigma = 1.0f / sigma;
  float a = static_cast<float>(sigma * sigma);

  fft(result);
  for (std::size_t f = 0; f < result.size(); ++f)
    result[f] *=
        std::exp(-0.5f * f * f / a) * Complex{0.0f, std::pow(2 * kPi * f, static_cast<float>(order))};
  ifft(result);

  return result.apply([](Complex c) -> Complex { return c.real(); })[std::slice(0, x.size(), 1)];
}

void gaussian(const std::vector<Coord>& src, double sigma,
              std::vector<Coord>& dst) {
  using util::lang::range;
  cv::Mat coord_x(static_cast<int>(src.size()), 1, CV_32F);
  cv::Mat coord_y(static_cast<int>(src.size()), 1, CV_32F);
  int size = static_cast<int>(src.size());

  auto it = std::begin(src);
  for (int k : range(0, size)) {
    coord_y.at<float>(k, 0) = static_cast<float>(it->i);
    coord_x.at<float>(k, 0) = static_cast<float>(it->j);
    ++it;
  }
  cv::Mat icoord_x, icoord_y;
  fft(coord_x, icoord_x);
  fft(coord_y, icoord_y);

  sigma = 1 / sigma;
  float a = static_cast<float>(sigma * sigma);
  for (int k : range(0, size)) {
    float k2 = static_cast<float>(k * k);
    auto x = icoord_x.at<std::complex<float>>(k, 0);
    auto y = icoord_y.at<std::complex<float>>(k, 0);
    icoord_x.at<std::complex<float>>(k, 0) = x * std::exp(-k2 / a);
    icoord_y.at<std::complex<float>>(k, 0) = y * std::exp(-k2 / a);
  }

  cv::Mat rx, ry;
  ifft(icoord_x, rx);
  ifft(icoord_y, ry);

  dst.clear();
  for (int k : range(0, size)) {
    int i = static_cast<int>(std::round(ry.at<float>(k, 0)));
    int j = static_cast<int>(std::round(rx.at<float>(k, 0)));
    dst.push_back(Coord(std::abs(i), std::abs(j)));
  }
}

void complex_magnitude(cv::InputArray src, cv::OutputArray dst) {
  CV_Assert(src.channels() == 2);
  std::vector<cv::Mat> planes(2);  // Real and imaginary parts
  cv::split(src, planes);
  cv::magnitude(planes[0], planes[1], planes[0]);
  planes[0].copyTo(dst);
}

void log_scale(cv::InputArray src, cv::OutputArray dst) {
  CV_Assert(src.type() == CV_32FC1);
  cv::Mat img = src.getMat();
  img += cv::Scalar::all(1);
  cv::log(img, img);
  cv::normalize(img, img, 0, 1, CV_MINMAX);
  img.copyTo(dst);
}

void combine(cv::InputArray left, cv::InputArray right, cv::OutputArray out) {
  CV_Assert(left.type() == right.type());
  cv::Size lsz = left.size();
  cv::Size rsz = right.size();
  cv::Size fsz(lsz.width + rsz.width, std::max(lsz.height, rsz.height));

  cv::Mat output(fsz, left.type());
  cv::Mat A(output, cv::Rect(0, 0, lsz.width, lsz.height));
  cv::Mat B(output, cv::Rect(lsz.width, 0, rsz.width, rsz.height));

  cv::Mat limg = left.getMat();
  cv::Mat rimg = right.getMat();
  cv::normalize(limg, limg, 0, 1, CV_MINMAX);
  cv::normalize(rimg, rimg, 0, 1, CV_MINMAX);
  limg.copyTo(A);
  rimg.copyTo(B);
  output.copyTo(out);
}

void load(const std::string& filename, cv::OutputArray image, int flags,
          int type) {
  cv::Mat input = cv::imread(filename, flags);
  if (input.empty()) {
    std::cerr << "Error: cannot open " << filename << "\n";
    std::exit(1);
  }
  input.convertTo(image, type);
  if (type == CV_32FC1) {
    cv::normalize(image, image, 0, 1, CV_MINMAX);
  }
}

bool save(const std::string& filename, cv::InputArray image) {
  cv::Mat img = image.getMat();
  cv::Mat output;
  img.convertTo(output, CV_8UC1);
  cv::normalize(output, output, 0, 255, CV_MINMAX);
  util::File(filename).ensure_path();
  return imwrite(filename, output);
}

void fill(cv::InputArray src, cv::OutputArray dst, Coord coord,
          uchar fill_color) {
  CV_Assert(src.type() == CV_8UC1);

  src.getMat().copyTo(dst);
  auto img = dst.getMat();

  if (!coord.within(img)) return;

  std::stack<Coord> stack;
  stack.push(coord);

  auto color = img.at<uchar>(stack.top());
  if (color == fill_color) return;

  while (!stack.empty()) {
    auto px = stack.top();
    stack.pop();

    if (img.at<uchar>(px) == color) {
      img.at<uchar>(px) = fill_color;

      if (px.j < img.cols - 1) stack.push(px + Coord{0, 1});
      if (px.j > 0) stack.push(px + Coord{0, -1});
      if (px.i < img.rows - 1) stack.push(px + Coord{1, 0});
      if (px.i > 0) stack.push(px + Coord{-1, 0});
    }
  }
}

cv::Rect extract_rectangle(const std::vector<Coord>& boundary) {
  constexpr int kIntMax = std::numeric_limits<int>::max();
  Coord min(kIntMax, kIntMax), max(0, 0);
  for (auto& it : boundary) {
    min = Coord(std::min(min.i, it.i), std::min(min.j, it.j));
    max = Coord(std::max(max.i, it.i), std::max(max.j, it.j));
  }
  return cv::Rect(min.j, min.i, max.j - min.j + 1, max.i - min.i + 1);
}

void extract_roi(const std::vector<Coord>& boundary, cv::InputArray src,
                 cv::OutputArray dst) {
  cv::Mat res(src.getMat(), extract_rectangle(boundary));
  res.copyTo(dst);
}

void extract_roi(const std::vector<Coord>& boundary, cv::InputArray src,
                 cv::OutputArray dst, int padding, int value) {
  CV_Assert(src.type() == CV_8UC1);
  cv::Mat res;
  extract_roi(boundary, src, res);
  copyMakeBorder(res, dst, padding, padding, padding, padding,
                 cv::BORDER_CONSTANT,
                 cv::Scalar_<uchar>::all(static_cast<uchar>(value)));
}

void erode(cv::InputArray src, cv::OutputArray dst, int erosion_size) {
  cv::Mat kernel = getStructuringElement(
      cv::MORPH_RECT, cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1),
      cv::Point(erosion_size, erosion_size));
  cv::erode(src, dst, kernel);
}

void dilate(cv::InputArray src, cv::OutputArray dst, int dilation_size) {
  cv::Mat kernel = getStructuringElement(
      cv::MORPH_RECT, cv::Size(2 * dilation_size + 1, 2 * dilation_size + 1),
      cv::Point(dilation_size, dilation_size));
  cv::dilate(src, dst, kernel);
}

// Copied from http://www.bytefish.de/blog/pca_in_opencv/
cv::Mat asRowMatrix(const std::vector<cv::Mat>& src, int rtype, double alpha,
                    double beta) {
  // Number of samples:
  std::size_t n = src.size();
  // Return empty matrix if no matrices given:
  if (n == 0) return cv::Mat();
  // dimensionality of (reshaped) samples
  size_t d = src[0].total();
  // Create resulting data matrix:
  cv::Mat data(static_cast<int>(n), static_cast<int>(d), rtype);
  // Now copy data:
  for (std::size_t i = 0; i < n; i++) {
    //
    if (src[i].empty()) {
      std::string error_message = cv::format(
          "Image number %d was empty, please check your input data.", i);
      CV_Error(CV_StsBadArg, error_message);
    }
    // Make sure data can be reshaped, throw a meaningful exception if not!
    if (src[i].total() != d) {
      std::string error_message = cv::format(
          "Wrong number of elements in matrix #%d! Expected %d was %d.", i, d,
          src[i].total());
      CV_Error(CV_StsBadArg, error_message);
    }
    // Get a hold of the current row:
    cv::Mat xi = data.row(static_cast<int>(i));
    // Make reshape happy by cloning for non-continuous matrices:
    if (src[i].isContinuous()) {
      src[i].reshape(1, 1).convertTo(xi, rtype, alpha, beta);
    } else {
      src[i].clone().reshape(1, 1).convertTo(xi, rtype, alpha, beta);
    }
  }
  return data;
}

}  // namespace ip
