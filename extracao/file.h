// Copyright © 2015, Filipe Verri and Paulo Urio.
#ifndef FILE_H_
#define FILE_H_

#include <string>

namespace util {

class File {
 public:
  explicit File(const std::string& filename);

  ~File() = default;

  std::string path() const;

  std::string ext() const;

  // Without path
  std::string basename() const;

  // Full filename
  const std::string& filename() const { return file_; }

  bool exists() const;

  bool ensure_path() const;

 private:
  static constexpr char kDirectorySeparator = '/';
  static constexpr char kExtensionSeparator = '.';

  const std::string file_;
};

}  // namespace util

#endif  // FILE_H_
