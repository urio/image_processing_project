// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:
#include "./datatable.h"

#include <numeric>
#include <algorithm>
#include <stdexcept>
#include <string>
#include <vector>
#include <tuple>

#include "./util.h"

namespace ml {

// Cell

Cell::Cell(const Cell& source) : type{source.type} {
  if (type == typeid(std::string))
    new (&s) std::string(source.s);
  else
    d = source.d;
}

Cell::Cell(Cell&& source) : type{source.type} {
  if (type == typeid(std::string))
    new (&s) std::string(std::move(source.s));
  else
    d = std::move(source.d);
}

Cell::Cell(const std::string& source) : type{typeid(std::string)} {
  new (&s) std::string(source);
}

Cell::Cell(std::string&& source) : type{typeid(std::string)} {
  new (&s) std::string(std::move(source));
}

Cell::Cell(double source) : type{typeid(double)} { d = source; }

Cell::Cell(const std::type_index& type) : type{type} {
  if (type == typeid(std::string))
    new (&s) std::string;
  else
    new (&d) double;
}

Cell::~Cell() {
  if (type == typeid(std::string)) s.~basic_string();
}

Cell& Cell::operator=(const Cell& source) {
  ml::Assert(type == source.type, "cells type differ");

  if (type == typeid(std::string))
    s = source.s;
  else
    d = source.d;

  return *this;
}

Cell& Cell::operator=(Cell&& source) {
  ml::Assert(type == source.type, "cells type differ");

  if (type == typeid(std::string))
    s = std::move(source.s);
  else
    d = std::move(source.d);

  return *this;
}

Cell& Cell::operator=(const std::string& source) {
  ml::Assert(type == typeid(source), "cells is not string");
  s = source;
  return *this;
}

Cell& Cell::operator=(std::string&& source) {
  ml::Assert(type == typeid(source), "cells is not string");
  s = std::move(source);
  return *this;
}

Cell& Cell::operator=(double source) {
  ml::Assert(type == typeid(source), "cells is not numeric");
  d = source;
  return *this;
}

Cell::operator double&() {
  ml::Assert(type == typeid(double), "cell is not numeric");
  return d;
}

Cell::operator std::string&() {
  ml::Assert(type == typeid(std::string), "cell is not string");
  return s;
}

Cell::operator double() const {
  ml::Assert(type == typeid(double), "cell is not numeric");
  return d;
}

Cell::operator const std::string&() const {
  ml::Assert(type == typeid(std::string), "cell is not string");
  return s;
}

bool Cell::is(const std::type_index& type) const { return this->type == type; }

std::ostream& operator<<(std::ostream& os, const Cell& cell) {
  if (cell.type == typeid(std::string))
    os << '"' << cell.s << '"';
  else
    os << cell.d;
  return os;
}

// Line Ranges and Iterators

LineIterator LineIterator::operator++(int) {
  LineIterator it = *this;
  ++row;
  return it;
}

LineIterator LineIterator::operator++() { return {++row, table}; }

LineIterator LineIterator::operator*() { return *this; }

bool LineIterator::operator!=(const LineIterator& other) {
  return row != other.row;
}

bool LineIterator::operator==(const LineIterator& other) {
  return !(*this != other);
}

Cell& LineIterator::at(std::size_t col) { return table->at(row, col); }

Cell& LineIterator::at(std::string col) { return table->at(row, col); }

LineIterator::LineIterator(std::size_t row, DataTable* table)
    : row{row}, table{table} {}

LineIterator LineRange::begin() { return {begin_, table_}; }

LineIterator LineRange::end() { return {end_, table_}; }

LineRange::LineRange(std::size_t begin, std::size_t end, DataTable* table)
    : begin_{begin}, end_{end}, table_{table} {}

// DataTable

DataTable::DataTable(std::initializer_list<Column> columns) {
  std::size_t i = 0;

  for (auto&& column : columns) {
    auto it = colindexes_.find(column.name);
    if (it != colindexes_.end()) throw TableException{};

    colnames_.push_back(column.name);
    colindexes_[column.name] = i++;
    coltypes_.push_back(column.type_index);
    data_.push_back(column.cells);
  }

  auto maxrows =
      std::accumulate(data_.begin(), data_.end(), std::size_t{},
                      [](std::size_t size, const std::vector<Cell>& column) {
                        return std::max(column.size(), size);
                      });

  auto type_it = coltypes_.begin();

  for (auto&& column : data_) {
    if (column.size() == 0) column.resize(maxrows, Cell{*type_it});

    if (column.size() != 0 && maxrows % column.size() != 0)
      throw TableException{};

    // for (auto&& cell : column)
    //  if (!cell.is(*type_it)) throw TableException{};
    ++type_it;

    if (column.size() != maxrows) column = rep_len(column, maxrows);
  }

  nrow_ = maxrows;
}

DataTable::size_type DataTable::nrow() const { return nrow_; }
DataTable::size_type DataTable::ncol() const { return data_.size(); }

bool DataTable::operator==(std::nullptr_t) const { return ncol() == 0; }

bool DataTable::operator!=(std::nullptr_t) const { return !(*this == nullptr); }

Cell& DataTable::at(size_type row, size_type col) {
  return data_.at(col).at(row);
}

const Cell& DataTable::at(size_type row, size_type col) const {
  return data_.at(col).at(row);
}

Cell& DataTable::at(size_type row, const std::string& col) {
  return data_.at(colindexes_.at(col)).at(row);
}

const Cell& DataTable::at(size_type row, const std::string& col) const {
  return data_.at(colindexes_.at(col)).at(row);
}

Cell& DataTable::operator()(size_type row, size_type col) {
  return data_[col][row];
}

const Cell& DataTable::operator()(size_type row, size_type col) const {
  return data_[col][row];
}

Cell& DataTable::operator()(size_type row, const std::string& col) {
  return data_[colindexes_.find(col)->second][row];
}

const Cell& DataTable::operator()(size_type row, const std::string& col) const {
  return data_[colindexes_.find(col)->second][row];
}

const std::vector<std::string>& DataTable::colnames() const {
  return colnames_;
}

void DataTable::reserve(size_type capacity) {
  for (auto&& column : data_) column.reserve(capacity);
}

DataTable& DataTable::rbind(std::initializer_list<Cell> row) {
  if (*this == nullptr) throw TableException{};
  if (row.size() != ncol()) throw TableException{};

  auto type_it = coltypes_.begin();
  for (auto&& cell : row) {
    if (!cell.is(*type_it)) throw TableException{};
    type_it++;
  }

  auto col_it = data_.begin();
  for (auto&& cell : row) {
    col_it++->push_back(cell);
  }

  ++nrow_;

  return *this;
}

DataTable& DataTable::rbind(const DataTable& table) {
  if (table == nullptr) {
    return *this;
  }

  if (*this == nullptr) {
    *this = table;
    return *this;
  }

  if (table.ncol() != ncol()) throw TableException{};

  for (std::size_t i = 0; i < ncol(); ++i)
    if (coltypes_[i] != table.coltypes_[i]) throw TableException{};

  nrow_ += table.nrow();
  for (std::size_t i = 0; i < ncol(); ++i) {
    data_[i].reserve(nrow_);
    data_[i].insert(data_[i].end(), table.data_[i].begin(),
                    table.data_[i].end());
  }

  return *this;
}

DataTable& DataTable::rbind(DataTable&& table) {
  if (table == nullptr) return *this;

  if (*this == nullptr) {
    *this = std::move(table);
    return *this;
  }

  if (table.ncol() != ncol()) throw TableException{};

  for (std::size_t i = 0; i < ncol(); ++i)
    if (coltypes_[i] != table.coltypes_[i]) throw TableException{};

  nrow_ += table.nrow();
  for (std::size_t i = 0; i < ncol(); ++i) {
    data_[i].reserve(nrow_);
    data_[i].insert(data_[i].end(),
                    std::make_move_iterator(table.data_[i].begin()),
                    std::make_move_iterator(table.data_[i].end()));
  }

  table = {};

  return *this;
}

DataTable& DataTable::cbind(const Column& col) { return cbind(DataTable{col}); }

DataTable& DataTable::cbind(Column&& col) {
  return cbind(DataTable{col});  // TODO: improve performance
}

DataTable& DataTable::cbind(const DataTable& table) {
  if (table == nullptr) return *this;

  if (*this == nullptr) {
    *this = table;
    return *this;
  }

  if (table.nrow() != nrow()) throw TableException{};

  for (auto&& name : table.colnames())
    if (colindexes_.find(name) != colindexes_.end()) throw TableException{};

  auto before = ncol();
  auto after = ncol() + table.ncol();

  data_.reserve(after);
  data_.insert(data_.end(), table.data_.begin(), table.data_.end());
  colnames_.insert(colnames_.end(), table.colnames_.begin(),
                   table.colnames_.end());
  coltypes_.insert(coltypes_.end(), table.coltypes_.begin(),
                   table.coltypes_.end());

  auto i = before;
  for (auto&& name : table.colnames()) colindexes_[name] = i++;

  return *this;
}

DataTable& DataTable::cbind(DataTable&& table) {
  if (table == nullptr) return *this;

  if (*this == nullptr) {
    *this = std::move(table);
    return *this;
  }

  if (table.nrow() != nrow()) throw TableException{};

  for (auto&& name : table.colnames())
    if (colindexes_.find(name) != colindexes_.end()) throw TableException{};

  auto before = ncol();
  auto after = ncol() + table.ncol();

  auto i = before;
  for (auto&& name : table.colnames()) colindexes_[name] = i++;

  data_.reserve(after);
  data_.insert(data_.end(), std::make_move_iterator(table.data_.begin()),
               std::make_move_iterator(table.data_.end()));
  colnames_.insert(colnames_.end(),
                   std::make_move_iterator(table.colnames_.begin()),
                   std::make_move_iterator(table.colnames_.end()));
  coltypes_.insert(coltypes_.end(),
                   std::make_move_iterator(table.coltypes_.begin()),
                   std::make_move_iterator(table.coltypes_.end()));

  ml::Assert(data_.size() == after && colnames_.size() == after &&
             coltypes_.size() == after && colindexes_.size() == after);

  table = {};

  return *this;
}

LineRange DataTable::lines() { return {0, nrow_, this}; }
LineRange DataTable::lines(size_type begin, size_type end) {
  return {begin, end, this};
}

const DataTable& DataTable::to_csv(std::ostream& os,
                                   const char* separator) const {
  if (*this == nullptr) return *this;

  decltype(colnames_) quoted;
  std::transform(colnames_.begin(), colnames_.end(), std::back_inserter(quoted),
                 [](const std::string& name) { return '"' + name + '"'; });

  util::paste(quoted.begin(), quoted.end(), os, separator);
  os << '\n';

  for (size_type i = 0; i < nrow(); ++i) {
    for (size_type j = 0; j < ncol() - 1; ++j) os << (*this)(i, j) << separator;
    os << (*this)(i, ncol() - 1) << '\n';
  }

  return *this;
}

// DataTable Static

template <typename OutputIt>
static inline OutputIt split(const std::string& string, OutputIt out,
                             const char* sep) {
  std::size_t ini = 0, pos = 0;

  while (pos != string.npos) {
    pos = string.find_first_of(sep, ini);
    *out++ = string.substr(ini, pos - ini);
    ini = pos + 1;
  }

  return out;
}

static inline bool is_number(const std::string& s) {
  return !s.empty() &&
         std::find_if(s.begin(), s.end(),
                      [](char c) { return !std::isdigit(c); }) == s.end();
}

DataTable DataTable::from_csv(std::istream& is, const char* separator) {
  DataTable table;

  std::string header;
  std::getline(is, header);
  split(header, std::back_inserter(table.colnames_), separator);

  int i = 0;
  for (auto&& colname : table.colnames_)
    table.colindexes_.insert(std::make_pair(colname, i++));

  auto ncol = table.colnames_.size();

  std::vector<std::string> data, csv_row;
  std::string line;

  while (std::getline(is, line)) {
    csv_row.resize(0);
    split(line, std::back_inserter(csv_row), separator);
    if (csv_row.size() != ncol)
      throw std::runtime_error{"invalid csv: number of columns differ"};

    for (auto&& col : csv_row) data.push_back(std::move(col));
  }

  auto nrow = data.size() / ncol;

  table.data_.resize(ncol);

  for (size_type col = 0; col < ncol; ++col) {
    if (is_number(data[col])) {
      for (size_type row = 0; row < nrow; ++row)
        table.data_[col].emplace_back(std::stod(data[row * ncol + col]));
      table.coltypes_.emplace_back(typeid(double));
    } else {
      for (size_type row = 0; row < nrow; ++row)
        table.data_[col].emplace_back(std::move(data[row * ncol + col]));
      table.coltypes_.emplace_back(typeid(std::string));
    }
  }

  table.nrow_ = nrow;

  return table;
}

std::ostream& operator<<(std::ostream& os, const DataTable& table) {
  table.to_csv(os, "\t");
  return os;
}

}  // namespace ml
