// Copyright © 2015, Filipe Verri and Paulo Urio.
#ifndef OPTIONS_H_
#define OPTIONS_H_

#include <cstdlib>

#include <algorithm>
#include <array>
#include <string>
#include <iostream>

#include "./util.h"

namespace options {

struct Opts {
  Opts(int argc, char* argv[]) : command(argv[0]) {
    if (argc != 2) print_help();
    input = std::string(argv[1]);
    check_input();
  }

  bool input_is_image() const {
    const std::array<std::string, 4> extensions = {{"jpg", "tif", "png"}};
    const std::string ext = util::File(input).ext();
    return std::any_of(
        extensions.begin(), extensions.end(),
        [&ext](const std::string& e) { return util::icompare(ext, e); });
  }

  bool input_is_csv() const {
    return util::icompare(util::File(input).ext(), "csv");
  }

  std::string command;
  std::string input;
  std::string output;

 private:
  void check_input() {
    if (!util::File(input).exists()) {
      std::cerr << "Input file " << input << " does not exist.\n";
      print_help();
    }
    if (!input_is_csv() && !input_is_image()) {
      std::cerr << "Input must be either a CSV or an image file.\n";
      print_help();
    }
  }

  void print_help() const {
    std::cerr << "Usage: " << command << " image_or_csv_file\n";
    std::exit(1);
  }
};

}  // namespace options

#endif  // OPTIONS_H_
