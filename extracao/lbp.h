// Copyright © 2015, Filipe Verri and Paulo Urio.
//
// Ojala, T., Pietikäinen, M., & Mäenpää, T. (2002). Multiresolution gray-scale
// and rotation invariant texture classification with local binary patterns.
// IEEE Transactions on Pattern Analysis and Machine Intelligence, 24(7),
// 971–987. doi:10.1109/TPAMI.2002.1017623
#ifndef LBP_H_
#define LBP_H_

#include <opencv2/core/core.hpp>

#include <vector>

#include "./circle.h"

namespace ml {

class LBP {
 public:
  enum class TextureOperator : int {
    GrayScaleInvariant = (1 << 0),
    RotationInvariant = (1 << 1),
    Uniform = (1 << 2),
    Interpolated = (1 << 3),
    GRIU2I = (GrayScaleInvariant | RotationInvariant | Uniform | Interpolated)
  };

  LBP(cv::InputArray src, cv::InputArray mask,
      ip::Circle_<double> circle = ip::Circle_<double>(8, 1.0),
      TextureOperator op = TextureOperator::GRIU2I);

  ~LBP() = default;

  const std::vector<double>& histogram() const { return histogram_; }

 private:
  void run();

  void apply_texture_operator();

  cv::Mat img_, mask_;
  ip::Circle_<double> circle_;
  TextureOperator op_;

  std::vector<double> histogram_;
};

}  // namespace ml

#endif  // LBP_H_
