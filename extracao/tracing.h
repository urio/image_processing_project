// Copyright © 2015, Filipe Verri and Paulo Urio.
#ifndef TRACING_H_
#define TRACING_H_

#include <opencv2/core/core.hpp>

#include <vector>

#include "./processing.h"
#include "./coord.h"

namespace ip {

class BoundaryTracing {
 public:
  // Boundary tracing with a |nb| neighborhood of radius 1.
  explicit BoundaryTracing(cv::InputArray src, Neighborhood nb);

  ~BoundaryTracing() = default;

  // Build inner boundary
  const std::vector<Coord> inner() const;

  // Build outer boundary
  const std::vector<Coord> outer() const;

  static cv::Mat to_mat(const std::vector<Coord>& boundary, cv::Size sz);

 private:
  void run();

  void init_neighborhood(Neighborhood nb);

  // Append |pt| to boundary and set it as the current tracking point.
  void update_current_point(const Coord& pt);

  void find_starting_point();

  void next();

  cv::Mat img_;
  int even_offset_;
  int odd_offset_;
  int dir_;   // Last direction
  Coord pt_;  // Current point
  std::vector<Coord> nb_;
  int nb_size_;

  std::vector<Coord> boundary_;
};

}  // namespace ip

#endif  // TRACING_H_
