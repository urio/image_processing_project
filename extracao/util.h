// Copyright © 2015, Filipe Verri and Paulo Urio.
#ifndef UTIL_H_
#define UTIL_H_

#include <numeric>
#include <algorithm>
#include <string>
#include <limits>
#include <iterator>
#include <utility>
#include <vector>
#include <cmath>
#include <iostream>

#include "./file.h"

namespace util {

// Case-insenstive string comparison
bool icompare(const std::string& a, const std::string& b);

template <class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
almost_equal(T x, T y) {
  return std::abs(x - y) < std::numeric_limits<T>::epsilon();
}

template <class T>
typename std::enable_if<std::numeric_limits<T>::is_integer, bool>::type
almost_equal(T x, T y) {
  return x == y;
}

template <
    class InputIt,
    typename T = typename std::iterator_traits<InputIt>::value_type,
    typename Distance = typename std::iterator_traits<InputIt>::difference_type>
auto unique_combinations(InputIt first, InputIt last)
    -> std::vector<std::pair<T, T>> {
  std::vector<std::pair<T, T>> result;
  std::size_t size = static_cast<std::size_t>(std::distance(first, last));
  result.reserve(size * (size - 1) / 2);
  for (InputIt a = first; a != last; ++a) {
    for (InputIt b = a; b != last; ++b) {
      result.emplace_back(*a, *b);
    }
  }
  return result;
}

template <class InputIt, class OutputIt,
          typename T = typename std::iterator_traits<OutputIt>::value_type>
void rescale(InputIt first, InputIt last, OutputIt d_first, T new_min,
             T new_max) {
  T min = *std::min_element(first, last);
  T max = *std::max_element(first, last);
  T c = (new_max - new_min) / (max - min) + new_min;
  std::transform(first, last, d_first,
                 [min, c](const T& v) -> T { return (v - min) * c; });
}

template <class InputIt, class OutputIt,
          typename T = typename std::iterator_traits<OutputIt>::value_type>
void pdf(InputIt first, InputIt last, OutputIt d_first) {
  T sum = std::accumulate(first, last, 0, [](T acc, T v) { return acc + v; });
  std::transform(first, last, d_first,
                 [sum](const T& v) -> T { return v / sum; });
}

template <class InputIt, class OStream, class Sep>
OStream& paste(InputIt first, InputIt last, OStream& os, const Sep& sep) {
  if (first != last) os << *first;
  for (InputIt it = ++first; it != last; ++it) os << sep << *it;
  return os;
}

}  // namespace util

#endif  // UTIL_H_
