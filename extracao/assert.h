// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:
#ifndef ASSERT_H_
#define ASSERT_H_

#include <exception>
#include <string>

namespace ml {

class Exception : std::exception {
 public:
  Exception(const std::string& message) : message{message} {}
  const char* what() const noexcept override { return message.c_str(); }

 private:
  std::string message;
};

template <typename Exc = Exception>
static inline void Assert(bool condition, const std::string& message = {}) {
#ifndef NDEBUG
  if (!condition) throw Exc{message};
#else
  (void)condition;
  (void)message;
#endif
}

}  // namespace ml

#endif  // ASSERT_H_
