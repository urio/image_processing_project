// Copyright © 2015, Filipe Verri and Paulo Urio.
#include "./symmetry.h"

#include <algorithm>

#include "./range.h"

namespace ml {

double Symmetry::vertical() const {
  using util::lang::range;

  double count = 0.0;
  for (int i : range(0, image_.rows / 2)) {
    for (int j : range(0, image_.cols)) {
      if (image_.at<uchar>(i, j) == image_.at<uchar>(image_.rows - i - 1, j))
        count++;
    }
  }
  return count * 2.0 / static_cast<double>(image_.size().area());
}

double Symmetry::horizontal() const {
  using util::lang::range;

  double count = 0.0;
  for (int i : range(0, image_.rows)) {
    for (int j : range(0, image_.cols / 2)) {
      if (image_.at<uchar>(i, j) == image_.at<uchar>(i, image_.cols - j - 1))
        count++;
    }
  }
  return count * 2.0 / static_cast<double>(image_.size().area());
}

double Symmetry::maximum() const { return std::max(vertical(), horizontal()); }

}  // namespace ml
