// Copyright © 2015, Filipe Verri and Paulo Urio.
// vim: set ts=2 sw=2:
#include "./pca.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cstddef>

#include <vector>

namespace {

// http://docs.opencv.org/trunk/d1/dee/tutorial_introduction_to_pca.html
static void drawAxis(cv::Mat& img, cv::Point p, cv::Point q, cv::Scalar colour,
                     const double scale) {
  double angle;
  double hypotenuse;
  angle = std::atan2((double)p.y - q.y, (double)p.x - q.x);  // angle in radians
  hypotenuse =
      sqrt((double)(p.y - q.y) * (p.y - q.y) + (p.x - q.x) * (p.x - q.x));
  // Here we lengthen the arrow by a factor of scale
  q.x = (int)(p.x - scale * hypotenuse * cos(angle));
  q.y = (int)(p.y - scale * hypotenuse * sin(angle));
  cv::line(img, p, q, colour, 1, CV_AA);
  // create the arrow hooks
  p.x = (int)(q.x + 9 * std::cos(angle + CV_PI / 4));
  p.y = (int)(q.y + 9 * std::sin(angle + CV_PI / 4));
  cv::line(img, p, q, colour, 1, CV_AA);
  p.x = (int)(q.x + 9 * std::cos(angle - CV_PI / 4));
  p.y = (int)(q.y + 9 * std::sin(angle - CV_PI / 4));
  cv::line(img, p, q, colour, 1, CV_AA);
}

}  // namespace

namespace ip {

PCA::PCA(const std::vector<Coord>& boundary, std::size_t components)
    : points_(boundary), components_(components) {
  compute();
}

// https://robospace.wordpress.com
// /2013/10/09/object-orientation-principal-component-analysis-opencv/
void PCA::compute() {
  // Construct a buffer used by the pca analysis
  cv::Mat data_pts(static_cast<int>(points_.size()), 2, CV_64FC1);
  for (int i = 0; i < data_pts.rows; ++i) {
    const std::size_t si = static_cast<std::size_t>(i);
    data_pts.at<double>(i, 0) = points_[si].j;
    data_pts.at<double>(i, 1) = points_[si].i;
  }

  // Perform PCA analysis
  cv::PCA pca(data_pts, cv::Mat(), CV_PCA_DATA_AS_ROW);

  // Store the position of the object
  object_pos_ = Coord(static_cast<int>(pca.mean.at<double>(0, 1)),
                      static_cast<int>(pca.mean.at<double>(0, 0)));

  // Store the eigenvalues and eigenvectors
  eigenvalues_.resize(components_);
  eigenvectors_.resize(components_);
  for (std::size_t i = 0; i < components_; ++i) {
    const int ii = static_cast<int>(i);
    eigenvectors_[i] = Coord_<double>(pca.eigenvectors.at<double>(ii, 1),
                                      pca.eigenvectors.at<double>(ii, 0));
    eigenvalues_[i] = pca.eigenvalues.at<double>(0, ii);
  }
}

double PCA::magnitude(std::size_t index) const {
  return (vectors()[index] * values()[index]).dist(center());
}

double PCA::angle() const { return axis(0).angle(); }

Coord PCA::axis(std::size_t index) const {
  return vectors()[index] * values()[index];
}

void PCA::draw_axes(cv::InputArray src, cv::OutputArray dst, double radius) {
  cv::Mat img;
  cv::cvtColor(src.getMat(), img, CV_GRAY2BGR);
  assert(img.type() == CV_8UC3);
  const cv::Scalar kColorCenter(CV_RGB(0, 0, 0xFF));
  const cv::Scalar kColor1(CV_RGB(0xFF, 0, 0));
  const cv::Scalar kColor2(CV_RGB(0, 0xFF, 0));
  CV_Assert(vectors().size() >= 2);
  cv::circle(img, center(), 3, kColorCenter, 2);
  Coord_<double> first(vectors()[0].i * values()[0],
                       vectors()[0].j * values()[0]);
  Coord_<double> second(vectors()[1].i * values()[1],
                        vectors()[1].j * values()[1]);
  double scale = (radius > 0.0) ? radius / magnitude(0) : 0.02;
  drawAxis(img, center(), Coord(first), kColor1, scale);
  drawAxis(img, center(), Coord(second * -1), kColor2, scale);
  img.copyTo(dst);
}

}  // namespace ip
