% vim: set spell spelllang=pt,en tw=80:

\documentclass[a4paper,final,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[portuges]{babel}

\usepackage{placeins}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{xparse}
\usepackage{xspace}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{colortbl}
\usepackage{textcomp}

\usepackage{color}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{import}
\usepackage{cleveref}
%\usepackage{showframe}

\crefname{figure}{Figura}{Figuras}
\crefname{table}{Tabela}{Tabelas}
\crefname{section}{Seção}{Seções}
\crefname{equation}{Equação}{Equações}
\newcommand{\crefrangeconjunction}{ à }

\newcommand{\zscore}{\mbox{$\mathrm{Z}$-Score}\xspace}

\usepackage{times}
\usepackage{titlesec}

\titleformat*{\section}{\large\bfseries}

\title{Projeto\\Processamento de Imagens}
\author{Filipe Verri (7152906) e Paulo Urio (9152455)}

\newcommand{\suchthat}{:}
\newcommand{\card}[1]{\left\lvert#1\right\rvert}

\newcommand{\Imagem}{I(x, y)}
\newcommand{\DominioObjeto}{\mathcal{D}}
\newcommand{\Contorno}{\mathcal{C}}
\newcommand{\Raio}{R}
\newcommand{\Sgn}[1]{\mathrm{sgn}(#1)}
\newcommand{\Round}[1]{\lfloor#1\rceil}

\NewDocumentCommand\MaxSet{m+g}{%
    \IfNoValueTF{#2}%
        {\max\!\left\{#1\right\}}%
        {\max\!\left\{#1,\;#2\right\}}%
}
\NewDocumentCommand\MinSet{m+g}{%
    \IfNoValueTF{#2}%
        {\min\!\left\{#1\right\}}%
        {\min\!\left\{#1,\;#2\right\}}%
}

\begin{document}

\maketitle

\section{Introdução}

Seja uma imagem $\Imagem$ em escalas de cinza que contém uma folha escaneada,
com fundo branco (uma folha de papel), em alguma escala, posição e rotação
arbitrária.  Cada imagem $\Imagem$ possui uma classe associada.  Dado um
conjunto de imagens conforme a descrição, os objetivos
deste projeto são:
\begin{itemize}
    \item Pré-processar e segmentar a folha em cada uma das imagens;
    \item Extrair descritores diversos e montar uma base de características;
    \item Aplicar técnicas de aprendizado de máquina na base obtida; e
    \item Avaliar os resultados obtidos.
\end{itemize}

O trabalho está dividido em extração e análise de características.  Na extração
de características, cada folha é processada em dois passos. Primeiro, como descrito
na \cref{sec:pp}, a imagem passa por um pré-processamento.  Segundo, detalhado
na \cref{sec:seg}, a imagem pré-processada passa por uma fase de segmentação e
extração de características.  Após aplicar esses passos para todas as folhas do
banco de dados, uma base de características é gerada para, então, sua a análise,
a qual é dividida em
classificação, \cref{sec:class}, estudo da distribuição, \cref{sec:dist}, e
agrupamento de dados, \cref{sec:clust}.  Por fim, o trabalho é concluído
na \cref{sec:conclusao}.

\section{Pré-processamento} \label{sec:pp}

Neste passo, alguns métodos de pré-processamento são aplicados à imagem
original, com o objetivo de eliminar ruídos, adquirir invariância à resolução
da imagem e preparar para aplicar os métodos de segmentação.
A seguir, é elaborado cada passo realizado durante o pré-processamento.

\paragraph{Ajuste de escala}
Utilizando a informação de resolução disponível nos metadados EXIF das imagens
escaneadas, as imagens foram inicialmente normalizadas para que todas fiquem
com a mesma resolução.  Com este ajuste, alguns descritores que dependem da
escala --- como perímetro e área de um objeto --- poderão ser usados na
classificação.

\paragraph{Acréscimo de borda}
Uma vez que o contorno da folha pode interceptar as bordas das imagens,
acrescentou-se uma borda branca de 3 pixels em cada imagem.  Deste modo, o
rastreamento do contorno poderá ser realizado considerando que todo o contorno
da folha está no interior da imagem.

\paragraph{Suavização}  O filtro gaussiano foi aplicado para
diminuir o ruído da imagem. Foi utilizada uma máscara $3 \times 3$ e $\sigma = 2$.
Equação da função gaussiana:
\[
    G(x) = \frac{1}{2\pi\sigma^2}e^{-\frac{x^2}{2\sigma^2}}.
\]


\section{Segmentação} \label{sec:seg}

A fase de segmentação tem o objetivo de detectar a folha contida na imagem já
pré-processada.  A seguir, detalhes de cada passo.

\paragraph{Binarização}  O método de limiarização de Otsu foi aplicado na imagem
em escala de cinzas para discretizar em valores binários:
\[
  b(x, y) = \begin{cases}
    1 & \text{se } (x, y) \in \DominioObjeto \\
      0 & \text{caso contrário,} \\
  \end{cases}
\]
onde $\DominioObjeto$ é o domínio que corresponde a região interna da folha.

\paragraph{Erosão e dilatação}
Após a limiarização, a imagem binária resultante pode ter pequenos erros, onde
uma região interna da folha pode estar marcada como fundo e vice-versa.
Estes erros podem invalidar o resultado do rastreamento do contorno.  Como
solução, utilizou-se os operadores morfológicos de erosão de dilatação.  Após a
erosão, falsos positivos são eliminados do fundo da imagem.  Já o processo de
dilatação é utilizado para preencher a região interna das imagens.  Ambos
operadores foram aplicados com uma máscara $3 \times 3$.

\paragraph{Rastreamento de contorno}

O método de rastreamento de contorno usado é baseado no esquema de cadeia de
códigos~\cite{Freeman1974,Jain1989}.  A partir de um ponto inicial do contorno do
objeto, uma cadeia de direções descreve o contorno do objeto que começa e termina
no mesmo ponto. O ponto inicial escolhido neste trabalho é o primeiro ponto do
contorno encontrado ao percorrer a partir do topo-esquerdo, ou seja,
$(x, y) \suchthat
  x \leq x' \land y \leq y' \;\forall (x, y), (x', y') \in \DominioObjeto$.
As direções são numeradas em sentido anti-horário, como apresentado na
\cref{fig:cadeia}.  O contorno final é uma sequência de pontos
$\Contorno \subseteq \DominioObjeto$.

\begin{figure}[ht]
  \centering
  %\includegraphics[scale=0.8]{imagens/cadeia-codigos}
  \import{imagens/}{cadeia-codigos.pdf_tex}
  \caption{Direções em uma cadeia de códigos de uma vizinhança oito-conectada.
    A partir de um ponto inicial, a direção é utilizada para descrever o
    próximo ponto que pertence à borda do objeto.}
  \label{fig:cadeia}
\end{figure}

\section{Extração de características} \label{sec:extraction}

Após a segmentação, a região e o contorno da folha já são conhecidos. Com
isso, várias características sobre a folha, como descritores de
forma~\cite{Costa2000} e de textura, foram extraídas:

\paragraph{Perímetro} Quantidade de pontos de contorno, $\card{\Contorno}$.
Como a imagem já foi normalizada pela escala, não é necessário realizar
nenhuma operação com a quantidade de pontos extraídos.

\paragraph{Área} A partir da imagem binária, a estimativa da área é a
quantidade de pontos no domínio do objeto, $A = \card{\DominioObjeto}$.

\paragraph{Raio} Maior distância do centro de massa $M$ para um ponto do
contorno,
\[
  \Raio = \MaxSet{ d(u, M) \suchthat u\in \Contorno } \mbox{,}
\]
onde $d(\cdot, \cdot)$ é uma métrica de distância entre dois pontos ---
neste trabalho foi utilizada a distância Euclidiana.

\paragraph{Diâmetro} Maior distância entre dois pontos do contorno,
\[
  D = \MaxSet{ d(u, v) \suchthat u,v\in \Contorno } \mbox{.}
\]

\paragraph{Distância média até a borda} Média das distâncias do centro de massa
$M$ para todos os pontos da borda,
\[
    D = \frac{1}{\card{\Contorno}}\sum_{u\in \Contorno}{d(u, M)} \, \mbox{.}
\]

\paragraph{Centro de massa} Média dos pontos do objeto,
  $\vec{r} \in \DominioObjeto$,
\[
  M =  \frac{1}{A} \sum_{\vec{r} \in \DominioObjeto} \vec{r} \, \mbox{.}
\]
O centro de massa foi normalizado para coordenadas relativas do objeto.


\paragraph{Simetria} Fração de pontos coincidentes do domínio do
objeto $\DominioObjeto$ e do domínio espelhado $\DominioObjeto^{*}$,
\[
    \frac{
        \card{\DominioObjeto \cap \DominioObjeto^{*}}
    }{
        \card{\DominioObjeto}
    }\, .
\]
Utilizou-se como descritores a simetria com espelhamento vertical e horizontal.

\paragraph{Maior e menor eixos} Utilizou-se a técnica de autovalores e
autovetores para encontrar o maior e o menor eixo do domínio $\DominioObjeto$.
% TODO: talvez explicar como
Os descritores extraídos das informações obtidas foram o centro do eixo, a razão
entre a magnitude dos eixos (alongamento) e o ângulo do maior eixo.

\newcommand{\Assinatura}{s(t)}
\newcommand{\Curvatura}{k(t)}
\newcommand{\Projecao}{r(t)}
\newcommand{\ContornoXY}{(x(t),y(t))}
\newcommand{\ContornoP}{c(t)}

\paragraph{Assinatura} Seja $\ContornoP = \ContornoXY$ a representação paramétrica do
contorno $\Contorno$. A assinatura do contorno é formada pelas distâncias da
borda ao centro de massa,
\[
  \Assinatura = d\big(\ContornoP, M\big) \, \mbox{.}
\]

\paragraph{Curvatura do contorno} É calculada por
\[
    \Curvatura = \frac{
        \dot x(t) \ddot y(t) - \ddot x(t) \dot y(t)
    }{
        \big(
            \dot x^2(t) + \dot y^2(t)
        \big) ^ {\frac 3 2}
      }\, \mbox{.}
\]
Neste trabalho, as derivadas foram calculadas no domínio da frequência
conjuntamente com um filtro passa-baixa com $\sigma = 0.1$. Seja $G(f)$ o sinal
$g(t)$ no domínio da frequência, a $a$-ésima derivada de $g$ no domínio da
frequência é
\[
    \frac{d^a g(t)}{d t^a} = {i\mkern1mu} (2\pi f)^a G(f) \,\mbox{.}
\]

\paragraph{{\itshape Bending energy}}
Expressa a energia necessária para transformar o contorno em um círculo,
\[
    B = \sum_t{\Curvatura^2}\,\mbox{.}
\]

\paragraph{{\itshape Local Binary Patterns} (LBP)} Para obter a informação de
textura da folha, foi extraído um histograma de ocorrência de padrões
uniformes locais, através da aplicação de um operador $T$ invariante à rotação
e à escalas de cinza~\cite{Tang1998}.
O operador $T$ na coordenada $c$ calcula a diferença entre o
nível de cinza $g_c$ do centro fixado e os níveis de cinza de uma vizinhança
circular.  Para a vizinhança circular, são extraídos $P$ pontos de um círculo
com raio $\Raio$.  O $k$-ésimo ângulo deste círculo é
\[
  \theta_k = \frac{2 \pi k}{P} \mbox{,}
\]
e o nível de cinza $g_k$ é determinado
pela interpolação bilinear de escalas de cinza dos pixels próximos à coordenada
\mbox{$c + (-\Raio \sin{\theta_k}, \Raio \cos{\theta_k})$}.
Seja a função de sinal
\[
  \Sgn{x} =
    \begin{cases}
      1 & \mbox{se } x \geq 0\mbox{,} \\
      0 & \mbox{se } x < 0\mbox{.}
    \end{cases}
\]
O operador de textura na coordenada $c$ é definido como
\[
  T_c =
  \begin{cases}
    \sum_{k=0}^{P-1} \Sgn{g_k - g_c} & \mbox{se } U_c \leq 2\mbox{,} \\
    P + 1 & \mbox{caso contrário,}
  \end{cases}
\]
onde $U_c = \sum_{k=1}^{P} | \Sgn{g_{k \bmod P} - g_c} - \Sgn{g_{k-1 \bmod
P} - g_c}| $ é a quantidade de variações de sinal na vizinhança circular.

Neste trabalho, uma vizinhança circular de 8 pontos com raio 1 foi considerada
para a extração de textura.  A imagem foi reduzida para 64 níveis de cinza.

\paragraph{Projeção anelar}

De forma similar a elaborada para LBP, para qualquer raio
$\gamma = \left[1, \Raio\right]$, a função de massa para um anel é definida
como
\[
  f(\gamma) = \sum_{k=0}^{P-1}
  b(\Round{ -\gamma \sin{\theta_k} },
    \Round{  \gamma \cos{\theta_k} }) \, \mbox{,}
\]
onde $P = 8 \gamma$ é a quantidade de pontos a serem extraídos do anel.
A \cref{fig:ringprojection} ilustra a função de massa pela projeção anelar.

\begin{figure}[ht]
  \centering
  \import {imagens/} {Hand-made-ring-projection.pdf_tex}
  \caption{Ilustração da assinatura por projeção anelar com $R$ anéis, para a
    imagem contendo a letra `A'.  Adaptado de Tang (1998)~\cite{Tang1998}.}
  \label{fig:ringprojection}
\end{figure}

\paragraph{Número de picos}
Número de cruzamentos em zero da segunda derivada do sinal.  Neste trabalho,
calculou-se o número de picos da curvatura, da assinatura e da projeção anelar
de cada uma das imagens.  O sinal unidimensional foi suavizado com $\sigma =
0.1$.

\paragraph{Descritor de Fourier}  Coeficientes da transformada de Fourier de um
sinal.  De cada uma das imagens, os valores absolutos dos dez primeiros
coeficientes das representações unidimensionais --- do contorno, da curvatura,
assinatura e projeção anelar --- foram utilizados como descritores.

\section{Exemplo}

Nesta seção, todos os passos de processamento de imagens --- pré-processamento,
segmentação e extração de características --- são ilustrados para uma única
folha escaneada.

\newcommand{\ExTam}{0.263}
\newcommand{\SubFigExTam}{0.327}
\begin{figure}[!ht]%
  \begin{subfigure}{\SubFigExTam\textwidth}
    \includegraphics[scale=\ExTam]{../extracao/exemplo/000-Input.jpg}
  \end{subfigure}%
  \begin{subfigure}{\SubFigExTam\textwidth}
    \includegraphics[scale=\ExTam]{../extracao/exemplo/001-Gaussian-Blur.jpg}
  \end{subfigure}%
  \begin{subfigure}{\SubFigExTam\textwidth}
    \includegraphics[scale=\ExTam]{../extracao/exemplo/002-Otsu.jpg}
  \end{subfigure}
  \begin{subfigure}{\SubFigExTam\textwidth}
    \includegraphics[scale=\ExTam]{../extracao/exemplo/003-Erosion.jpg}
  \end{subfigure}%
  \begin{subfigure}{\SubFigExTam\textwidth}
    \includegraphics[scale=\ExTam]{../extracao/exemplo/004-Dilation.jpg}
  \end{subfigure}%
  \begin{subfigure}{\SubFigExTam\textwidth}
    \includegraphics[scale=\ExTam]{../extracao/exemplo/005-Inner-boundary.jpg}
  \end{subfigure}%
  \caption{Do topo esquerdo à direita: exemplo de imagem de entrada;
    filtro gaussiano; imagem discretizada em valores binários com o método de
    Otsu; aplicação dos operadores morfológicos de erosão e dilatação; e
    contorno extraída a partir da imagem binarizada.}
  \label{fig:segmentacao}
\end{figure}

Na \cref{fig:segmentacao}, as etapas do pré-processamento e segmentação são
exemplificadas por diferentes imagens.  A seguir, a \cref{tab:features} resume
diversos descritores de forma obtidos da imagem.  O menor e o maior eixo são
mostrados na \cref{fig:axes}. Os descritores de Fourier dos
padrões unidimensionais são apresentados na \cref{tab:fourier}.  Por fim, o
histograma normalizado de padrões acumulados pelo método LBP são indicados na
\cref{fig:lbp}.

\begin{figure}[!ht]
\begin{minipage}{0.45\textwidth}
  \centering
  \captionof{table}{Características extraídas da folha utilizada como
    exemplo.}
  \label{tab:features}
  \input {../extracao/exemplo/features.tex}
\end{minipage}%
\hfill
\begin{minipage}{0.5\textwidth}
  \centering
  \includegraphics[scale=0.38]{../extracao/exemplo/007-PCA-Axes.jpg}
  \caption{Coordenada central, menor e maior eixo obtido pelo PCA.}
  \label{fig:axes}
\end{minipage}
\end{figure}
\FloatBarrier

\begin{table}[!ht]
  \centering
  \caption{Descritores de Fourier de informações extraídas da folha utilizada
  como exemplo.  O valor do coeficiente é representado pela intensidade em
escalas de cinza, onde branco representa maior magnitude. Valores foram
normalizados em escala logarítmica.}
  \label{tab:fourier}
  \input {../extracao/exemplo/fourier.tex}
\end{table}

\begin{figure}[!ht]
  \centering
  \includegraphics[scale=0.5]{../extracao/exemplo/lbp.pdf}
  \caption{Histograma de ocorrência padrões uniformes locais obtido pelo método
      {\itshape Local Binary Patterns} (LBP) para a imagem utilizada como
    exemplo. }
  \label{fig:lbp}
\end{figure}

\FloatBarrier

\section{Classificação} \label{sec:class}

\newcommand{\EBetula}{{\itshape Betula pendula}\xspace}
\newcommand{\ENerium}{{\itshape Nerium oleander}\xspace}
\newcommand{\EOlea}{{\itshape Olea europaea}\xspace}
\newcommand{\EQuercus}{{\itshape Quercus ilex}\xspace}

\newcommand{\EB}{{\itshape B.~pendula}\xspace}
\newcommand{\EN}{{\itshape N.~oleander}\xspace}
\newcommand{\EO}{{\itshape O.~europaea}\xspace}
\newcommand{\EQ}{{\itshape Q.~ilex}\xspace}

\newcommand{\Especies}{\EBetula, \ENerium,  \EOlea e \EQuercus}

Considere $T = \{x_i, y_i\}$ o conjunto de dados obtido pelos passos descritos
anteriormente.  O vetor $x_i \in \mathcal{R}^D$ representa os $D$ descritores da
$i$-ésima imagem.  O rótulo $y_i$, por sua vez, representa a espécie da planta.
Foram utilizadas 412 imagens de plantas das espécies \Especies. Exemplos
para cada uma das espécies são apresentados na \cref{fig:especies}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.2\textwidth]{imagens/pendula}
    \includegraphics[width=0.2\textwidth]{imagens/oleander}
    \includegraphics[width=0.2\textwidth]{imagens/europaea}
    \includegraphics[width=0.2\textwidth]{imagens/ilex}
    \caption{
      Exemplos de imagens de plantas a usados neste trabalho.
        Da esquerda para direita: \Especies.
    }
    \label{fig:especies}
\end{figure}

Nesta seção, os resultados obtidos da classificação deste conjunto são
apresentados.  O processo de aprendizado foi dividido nas etapas de
normalização, redução de dimensionalidade e classificação:

\paragraph{Normalização} Atributos em que o intervalo de valores é muito extenso
em relação aos outros podem influenciar muito os classificadores.  Por conta
disso, nesta etapa, os vetores $x_i$ foram reescalados utilizando a normalização
\zscore e a normalização no intervalo $[-1,+1]$.

\paragraph{Redução de dimensionalidade} A quantidade de dimensões tem grande
impacto no desempenho de um classificador, pois o espaço de busca aumenta
exponencialmente com o número de dimensões.  Em vista disso, a dimensão dos
vetores $x_i$ foi reduzida aplicando técnicas de seleção de características.
Foram usados os filtros {\itshape Correlation-based feature selection}
(CFS)~\cite{Hall1998} e {\itshape Consistency-based search}~\cite{Dash2003},
ambos com percurso {\itshape best-first}.
Após a filtragem, denota-se $D^{*}$ a nova dimensionalidade dos
vetores $x_i$.

\paragraph{Classificação} Com os dados preparados, o processo de classificação
foi realizado com 10 execuções independentes de validação cruzada
estratificada de 10 pastas.  As técnicas selecionadas foram
  {\itshape \emph{k}-Nearest Neighbors} (\emph{k}-NN),
  {\itshape Gaussian Naïve Bayes},
  {\itshape Multilayer Perceptron} (MLP) e
  {\itshape Support Vector Machines} (SVM).
  Utilizou-se $k = 3$ para o algoritmo \emph{k}-NN,
  uma única camada escondida com $D^{*}+1$ neurônios para a rede neural MLP e
  um núcleo radial para SVM.

\begin{figure}[!ht]
    \includegraphics[width=\textwidth]{./imagens/class-nlbp.pdf}
    \caption{
        Resultado da classificação utilizando apenas descritores de forma.
    }
    \label{fig:class-nlbp}
\end{figure}

Na \cref{fig:class-nlbp}, os resultados do processo de classificação usando
apenas descritores de forma são apresentados.  No total, foram extraídos 57
descritores, sem normalizar, dos quais foram selecionados:
\begin{itemize}
    \item 15 usando a maior correlação com a classe: raio; primeira coordenada do
        eixo; alongamento; número de picos da curvatura e da assinatura; 2º e 10º
        descritores de Fourier da curvatura; 2º, 3º, 4º, 9º e 10º descritores de
        Fourier da projeção anelar; 3º descritor de Fourier da assinatura; e 3º
        e 8º descritores de Fourier do contorno.
    \item 8 usando melhor consistência com a classe: perímetro; área; primeira
        coordenada do eixo; 1º, 3º e 7º descritores de Fourier do contorno; e 2º e
        9º descritores de Fourier da projeção anelar.
\end{itemize}

Observa-se que os resultados com o filtro baseado em consistência possuem menor
acurácia média, porém menos características são usadas.  A normalização melhorou
significantemente os resultados do 3-NN e, principalmente, da rede MLP.  A
melhora no desempenho do classificador {\itshape Naïve Bayes} ao normalizar os
dados deve-se, principalmente, à melhor escolha das características. A técnica SVM
obteve os melhores resultados.

\begin{figure}[!ht]
    \includegraphics[width=\textwidth]{./imagens/class-lbp.pdf}
    \caption{
        Resultado da classificação utilizando apenas o descritor de textura LBP.
    }
    \label{fig:class-lbp}
\end{figure}

De maneira análoga, o experimento foi repetido utilizando apenas o LBP como
descritor de textura.  Os resultados estão na \cref{fig:class-lbp}.  Neste caso,
a normalização e redução de dimensionalidade não melhora os resultados.  Uma vez
que, todos os descritores estão na mesma escala e deveriam ter a mesma
importância.

\begin{figure}[!ht]
    \includegraphics[width=\textwidth]{./imagens/class.pdf}
    \caption{
        Resultado da classificação utilizando todos os descritores extraídos.
    }
    \label{fig:class}
\end{figure}

Conforme observado na \cref{fig:class}, o uso de todos os descritores reduz
significantemente a acurácia do método 3-NN.  Isto ocorreu, principalmente, pela
diferença de grandeza entre os descritores --- observe que a normalização dos
dados resolveu o problema.  Nestas condições de execução, as técnicas 3-NN, MLP
e SVM obtiveram resultados similares.  A técnica {\itshape Naïve Bayes} obteve
acurácia pouco inferior.


\begin{table}[!ht]
    \caption{Matriz de confusão do processo de classificação utilizando 3-NN, CFS
    e \zscore.}
    \label{tab:nn}
    \centering
    \begin{tabular}{l r r r r}
    &\EB&\EN&\EO&\EQ\\\
    \EB&728 & 0 & 0 & 32 \\
    \EN&0 & 870 & 10 & 0 \\
    \EO&4 & 3 & 1240 & 3 \\
    \EQ&24 & 0 & 14 & 1192 \\
    \end{tabular}
\end{table}


\begin{table}[!ht]
    \caption{Matriz de confusão do processo de classificação utilizando MLP e
    \zscore.}
    \label{tab:mlp}
    \centering
    \begin{tabular}{l r r r r}
    &\EB&\EN&\EO&\EQ\\\
    \EB&724 & 1 & 11 & 24 \\
    \EN&0 & 864 & 13 & 3 \\
    \EO&2 & 4 & 1234 & 10 \\
    \EQ&9 & 2 & 19 & 1200 \\
    \end{tabular}
\end{table}

\begin{table}[!ht]
    \caption{Matriz de confusão do processo de classificação utilizando Naïve
        Bayes,  CFS e \zscore.}
    \label{tab:nb}
    \centering
    \begin{tabular}{l r r r r}
    &\EB&\EN&\EO&\EQ\\\
    \EB&631 & 0 & 3 & 126 \\
    \EN&0 & 843 & 37 & 0 \\
    \EO&10 & 35 & 1174 & 31 \\
    \EQ&48 & 0 & 36 & 1146 \\
    \end{tabular}
\end{table}

\begin{table}[!ht]
    \caption{Matriz de confusão do processo de classificação utilizando SVM, CFS
    e \zscore.}
    \label{tab:svm}
    \centering
    \begin{tabular}{l r r r r}
    &\EB&\EN&\EO&\EQ\\\
    \EB & 740 & 0 & 0 & 20 \\
    \EN & 0 & 869 & 11 & 0 \\
    \EO & 5 & 8 & 1228 & 9 \\
    \EQ & 17 & 0 & 10 & 1203 \\
    \end{tabular}
\end{table}

As matrizes de confusão dos melhores resultados obtidos por cada técnicas são
apresentados nas \cref{tab:nn,tab:mlp,tab:nb,tab:svm}.  O resultado das  10
execuções independentes foram somados.

\FloatBarrier

\section{Distribuição dos dados}
\label{sec:dist}

Nesta seção, as distribuições estimadas das quatro classes escolhidas ---
\Especies\ --- são apresentadas.

Para estimar as probabilidades {\itshape a priori}, utilizou-se inferência
frequentista.  Ou seja, a probabilidade {\itshape a priori} $P(Y = y)$ é calculada pela
quantidade de ocorrência da espécie $y$ dividido pelo total de observações.
As probabilidades {\itshape a priori} são apresentadas na \cref{tab:priori}.

\begin{table}[!ht]
    \caption{Distribuição de probabilidade {\itshape a priori} das espécies de
    plantas.}
    \label{tab:priori}
    \centering
    \begin{tabular}{r l}
        $P(Y = \text{\EB}) =$ & $0.184$ \\
        $P(Y = \text{\EN}) =$ & $0.214$ \\
        $P(Y = \text{\EO}) =$ & $0.303$ \\
        $P(Y = \text{\EQ}) =$ & $0.299$
    \end{tabular}
\end{table}

A verossimilhança $P(X|Y)$, por sua vez, foi calculada utilizando o método de
estimação de misturas normais descrito em~\cite{Fraley2007}.  Para a
visualização dos resultados, apenas dois descritores foram utilizados em cada
estimação.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.43\textwidth]{imagens/random-all.pdf}
    \includegraphics[width=0.43\textwidth]{imagens/pca-all.pdf}
    \includegraphics[width=0.43\textwidth]{imagens/relief-all.pdf}
    \caption{
        Projeções do conjunto $T$ com diferentes características. As cores dos
        pontos indicam a classe.  As cores vermelha, verde, azul e ciano
        representam, nesta ordem, as espécies \EB, \EN, \EO, \EQ.
    }
    \label{fig:vis}
\end{figure}


Na \cref{fig:vis}, o conjunto de dados $T$ é projetado com diferentes
características.  No primeiro gráfico, utilizou-se as características área e
perímetro com normalização \zscore.  No segundo, as componentes do PCA foram
utilizadas.  No terceiro gráfico, as características escolhidas são o valor
absoluto primeiro descritor de Fourier da projeção anelar e o alongamento também
normalizados com técnica \zscore.  As cores vermelha, verde, azul e ciano
representam, nesta ordem, as espécies \EB, \EN, \EO, \EQ.


\begin{figure*}[!ht]
    \centering
    \includegraphics[height=0.8\textheight]{imagens/random-density.pdf}
    \caption{
        Estimação da verossimilhança $P(X|Y)$ da área e do perímetro dado a
        espécie. As cores dos
        pontos indicam a classe.
        As cores vermelha, verde, azul e ciano
        representam, nesta ordem, as espécies \EB, \EN, \EO, \EQ.
    }
    \label{fig:drandom}
\end{figure*}

\begin{figure*}[!ht]
    \centering
    \includegraphics[height=0.8\textheight]{imagens/pca-density.pdf}
    \caption{
        Estimação da verossimilhança $P(X|Y)$ dos dois primeiros componentes do
        PCA dado a espécie. As cores dos
        pontos indicam a classe.
        As cores vermelha, verde, azul e ciano
        representam, nesta ordem, as espécies \EB, \EN, \EO, \EQ.
    }
    \label{fig:dpca}
\end{figure*}

\begin{figure*}[!ht]
    \centering
    \includegraphics[height=0.8\textheight]{imagens/relief-density.pdf}
    \caption{
        Estimação da verossimilhança $P(X|Y)$ do primeiro descritor de Fourier
        da projeção anelar e do alongamento dado a espécie. As cores dos
        pontos indicam a classe.  As cores vermelha, verde, azul e ciano
        representam, nesta ordem, as espécies \EB, \EN, \EO, \EQ.
    }
    \label{fig:drelief}
\end{figure*}
\FloatBarrier
Nas \cref{fig:drandom,fig:dpca,fig:drelief}, a distribuição da verossimilhança
dado cada uma das classes são apresentados para cada um dos cenários descritos.
Nota-se que, utilizando área e perímetro, diferentes espécies não ocorrem
separadas no espaço.  Além disso, estes descritores são correlacionados.  No
entanto, com os dois primeiros componentes do PCA, os dados ficam melhor
distribuídos, mas com significante sobreposição nos exemplos das classes \EN,
\EO e \EQ.  Por fim, com os dois descritores mais significativos, apenas há
sobreposição significativa entre as classes \EN e \EO.\FloatBarrier

\section{Agrupamento}
\label{sec:clust}

Nesta última etapa do projeto, a técnica \emph{k}-médias foi aplicada sobre o
conjunto de dados $T$.  Para a avaliação dos resultados, utilizou-se medidas de
validação interna e externa.

O Índice C e a média da Silhueta foram as medidas de validação interna calculadas.  O
Índice C indica o grau de proximidade dos exemplos no mesmo agrupamento em
relação às distâncias de toda a base.  Seus valores estão no intervalo $[0, 1]$,
em que $0$ representa o melhor agrupamento.  A Silhueta, por sua vez, pondera a
densidade intragrupos e a separabilidade intergrupos.  Seu intervalo compreende
os valores no intervalo $[-1, 1]$, sendo $1$ o melhor caso.

Como índices de validação externa, utilizou-se o coeficiente de Jaccard e o
índice de Rand.  Ambos resultam em valores no intervalo $[0, 1]$, em que $1$
representa total conformidade com os rótulos desejados.  O coeficiente de
Jaccard mede a similaridade entre conjuntos finitos e o índice de Rand a chance
de dois exemplos da mesma classe pertencerem a um mesmo grupo.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=1.02\textwidth]{./imagens/clus.pdf}
    \caption{
        Resultado do agrupamento utilizando a técnica $k$-médias.
    }
    \label{fig:clus}
\end{figure}

Os resultados são apresentados na \cref{fig:clus}.  Destaca-se o caso em que as
características não foram normalizadas.  Neste cenário, o Índice C obteve
resultado muito próximo do ótimo.  Isto ocorre devido aos casos degenerados da
equação do índice (consulte \cite{Hubert1976} para mais detalhes).  Ainda neste
cenário e utilizando todas as características, a Silhueta não pôde ser calculada
devido à magnitude das distâncias.

Além disso, é importante reforçar que os valores do Índice C e da Silhueta não
podem ser comparadas entre diferentes tipos de normalização e seleção de
características, uma vez que toda a métrica de comparação é alterada.

Quanto à qualidade do resultado considerando os índices externos, a técnica
\emph{k}-médias obteve boa performance, especialmente com os dados normalizados.

\section{Conclusões} \label{sec:conclusao}

Neste projeto, uma base de imagens de folhas escaneadas foi pré-processada e as
folhas segmentadas.  Em seguida, descritores de forma e textura foram extraídos
e organizados em uma base de características.  Por fim, técnicas de aprendizado
de máquina foram aplicadas na base.

Na etapa de pré-processamento, pouco tratamento foi necessário.  As
imagens foram reescaladas para que ficassem na mesma resolução.  Com isso,
descritores simples como área e perímetro mostraram-se úteis no aprendizado.
Além da reescala, as imagens foram acrescidas de uma borda e também suavizadas.

O método de Otsu foi utilizado para a segmentação da imagem.  Uma vez que as
imagens são simples, este método obteve bons resultados para todas as imagens.
No entanto, algumas das imagens apresentaram pequenos buracos, ou
aglomerados, fora do domínio do objeto.  Estes aglomerados causam problemas
no método de rastreamento.  Para resolver este problema, utilizou-se os
operadores morfológicos de erosão e de dilatação.

Quanto à extração de características, foram obtidos 57 descritores de forma e um
descritor de textura contendo 10 valores.  Apesar de menor quantidade, o
histograma de ocorrência de padrões uniformes locais do LBP contém informação
suficiente para se atingir boa acurácia na classificação.  Ademais, alguns
descritores de forma se destacaram.  Por exemplo, o alongamento e os descritores
de Fourier da projeção anelar.  Observando a \cref{fig:especies}, percebemos que
uma das espécies é mais alongada que as demais.  Já os descritores da projeção
anelar são invariantes à rotação e à translação, mas não à escala, o que é
desejado para o problema.

Na etapa de aprendizado, notou-se a importância da normalização e redução de
dimensionalidade.  Porém, não houve grande diferença entre os tipos de
normalização e técnicas de seleção de características.  Quando tratados os
dados, as técnicas de classificação e de agrupamento obtiveram bons resultados.
Isto reforça a qualidade das características extraídas.

\bibliographystyle{plain}
\bibliography{referencias}

\end{document}
