#! /usr/bin/env Rscript
# Copyright © 2015, Filipe Verri and Paulo Urio.
# vim: set ft=r:

args <- commandArgs(trailingOnly = TRUE)

stopifnot(length(args) < 3)

if (!interactive()) {
    input <- if (length(args) > 0) args[1] else file("stdin")
    output <- if (length(args) > 1) args[2] else "clustering.pdf"
} else {
    input <- "clus.tab"
    output <- NULL
}

require("ggplot2", quietly = TRUE)
require("plyr", quietly = TRUE)

# Classification
data <- read.table(input, header = TRUE)

data$filter <- revalue(data$filter, c("all.features" = "Todas",
                                      "consistency.filter" = "Consistência"))
data$normalization <- revalue(data$normalization,
                              c("none" = "Nenhuma",
                                "zscore" = "Z-Score",
                                "between.-1.and.1" = "Entre -1 e 1"))
data$normalization <- factor(data$normalization, levels(data$normalization)[c(2, 1, 3)])
data$index <- revalue(data$index, c("jaccard" = "Jaccard",
                                    "rand" = "Rand",
                                    "c_index" = "Índice C",
                                    "silhouette" = "Silhueta"))
data$index <- factor(data$index, levels(data$index)[c(1,4,2,3)])
#data$mean.value[data$normalization == "Nenhuma" & data$index %in% c("Índice C", "Silhueta")] <- NA

p <- ggplot(data, aes(x = index, y = mean.value, fill = index,
                      label = sprintf("%.2f", mean.value),
                      color = index,
                      ymin = mean.value - sd.value,
                      ymax = mean.value + sd.value)) +
     #geom_crossbar() +
     geom_bar(stat = "identity", position = "dodge") +
     #geom_point(size = 10) +
     geom_errorbar(color = "#444444", width = 0.25) +
     #geom_text() +
     facet_grid(normalization ~ filter) +
     ylab("Acurácia") + xlab("") +
     theme(legend.position = "none")

if (is.null(output)) {
    plot(p)
} else {
    ggsave(output, p, width = 10, height = 5)
}
